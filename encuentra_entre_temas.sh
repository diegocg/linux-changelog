#!/bin/bash
res1=$(cat oneline.txt | grep "$1" | awk '{print $1}')
res2=$(cat oneline.txt | grep "$2" | awk '{print $1}')
if [ -z "$res1" ]
then
    echo "first parameter is empty"
    exit
fi

if [ -z "$res2" ]
then
    echo "second parameter is empty"
    exit
fi
res1=$(echo $res1 | awk '{print $1}')
res2=$(echo $res2 | awk '{print $1}')
echo $res1
echo $res2
./rangeformat.sh $res1 $res2
