module infoDeTema;

import lei : encuentraTemaLei;
import útiles : sacaTemaDeListaCambios, dameLog;
import conf : gitRepo, sitio;

void main(string[] args) {
    import std.algorithm : map;
    import std.uni : toLower;
    import std.array : array;
    import std.conv : text;

    bool quiereLargo = true;

    string tema = sacaTemaDeListaCambios(args[1]);
    string[] log = dameLog(gitRepo).map!toLower.array;
    auto temaLei = encuentraTemaLei(tema, log, gitRepo, sitio);

    imported!"std.stdio".writeln(temaLei);
}
