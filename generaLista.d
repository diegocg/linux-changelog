module get_changelog;

import std.stdio : writeln;

import commit : Commit ;
import lista : generaListaConRelaciones;
import serie : Serie;
import mantenedores : seccionaYFiltraArchivos, Entrada, ÁreaMantenida;
import útiles : seriesDeCommits, commitsDeShas, commitsDeCorreos, dameLog;

import conf : mailPath, gitRepo, sitio, codeRepo;

void main(string[] args) {
    import std.stdio;
    import std.algorithm : map;
    import std.uni : toLower;
    import std.array : array;

    string path = args.length > 1 ? args[1] : mailPath;
    string[] log = dameLog(gitRepo).map!toLower.array;

    Commit[] commits = commitsDeShas(gitRepo, commitsDeCorreos(path));
    Serie[] ser = seriesDeCommits(commits, log, gitRepo, sitio);

    ÁreaMantenida[] maint = seccionaYFiltraArchivos(codeRepo);
    ÁreaMantenida uapi = {nombre: "uapi", entradas: [Entrada('F', "include/uapi")]};
    maint ~= uapi;

    string lista = generaListaConRelaciones(ser, maint, gitRepo, sitio);
    writeln("Lista final\n", lista);
}
