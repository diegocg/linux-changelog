module infodetema;

import lei : encuentraTemaLei;
import útiles : dameLog, sacaTemaDeListaCambios;
import conf : gitRepo, sitio;

void main(string[] args) {
    import std.algorithm : map;
    import std.uni : toLower;
    import std.array : array;
    import std.conv : text;

    string tema = sacaTemaDeListaCambios(args[1]);
    string[] log = dameLog(gitRepo).map!toLower.array;

    auto temaLei = encuentraTemaLei(tema, log, gitRepo, sitio);

    imported!"std.stdio".writeln(temaLei);
}
