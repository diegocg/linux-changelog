module pruebas;

import útiles : dameLog;
import commit : Commit;
import serie : Serie;
import tema : Tema;
import diferencia : juntaDiferencias;
import útiles : asuntosDeCorreos, seriesDeAsuntos;
import conf : gitRepo, sitio, codeRepo;
import mantenedores : ÁreaMantenida;

import std.stdio : writeln;
import std.path : buildPath;
import unit_threaded.should;

// dameLog es muy costoso, sólo debemos hacerlo una vez para todas
// las pruebas, que se ejecutan en paralelo
string[] logBuf() {
    static shared string[] log;

    synchronized {
        if (log == []) {
            log = cast (shared) dameLog(gitRepo);
        }
    }

    return cast (string[])log;
}

ÁreaMantenida[] sacarÁreasDeSerie(Serie ser) {
    import conf : gitRepo, sitio, codeRepo;
    import mantenedores : parseMaintainers, filterMaintainers;
    import lista : filtraÁrea;
    import std.path : buildPath, dirName;

    string[] log = dameLog(gitRepo);
    string maintPath = buildPath(dirName(__FILE__), "MAINTAINERS");
    auto maint = parseMaintainers(maintPath).filterMaintainers(['F']);
    auto ret = filtraÁrea(maint, ser);
    return ret;
}

void detallesSerie(Serie s) {
    writeln("Serie nivel ", s.nivel);
    foreach(t; s.temas) {
        string h = t.commit.isNull ? "" : " sha " ~ t.commit.get.hash;
        writeln("Tema ", t.tema, " corto ", t.asuntoLimpio, h);
    }
    foreach(dif; s.diferencias) {
        writeln("Dif: ", dif);
    }
}

bool temasTienenCommit(Serie s, int[] temasConCommit) {
    import std.algorithm : maxElement, canFind;
    const temasMax = temasConCommit.maxElement;
    const serieMax = s.temas.keys.maxElement;
    if (temasMax > serieMax) {
        writeln("Max temas ", temasConCommit, " es más grande que los correos ", s.temas.keys);
        return false;
    }

    foreach(i, t; s.temas) {
        if (canFind(temasConCommit, i)) {
            if (t.commit.isNull) {
                writeln("Error: Tema ", i, " ", t, " con commit ", t.commit, " nulo, cuando debería tener algo");
                return false;
            }
        } else {
            if (!t.commit.isNull) {
                writeln("Error: Tema ", i, " ", t, " con commit ", t.commit, " lleno, cuando debería ser nulo");
                return false;
            }
        }
    }
    return true;
}

@("limpiaAsunto")
unittest {
    import std.exception : enforce;
    import tema : limpiaAsunto;

    string tema1 = "Re: [PATCH net 0/3] net: phy: mscc: support VSC8501";
    string limpio1 = "phy: mscc: support VSC8501";
    tema1.limpiaAsunto.should == limpio1;

    string tema2 = "[PATCH net v3 1/4] net: phy: mscc: add VSC8502 to MODULE_DEVICE_TABLE";
    string limpio2 = "phy: mscc: add VSC8502 to MODULE_DEVICE_TABLE";
    tema2.limpiaAsunto.should == limpio2;

    string tema3 = "[PATCH v4 00/16] Enable DSA 2.0 Event Log and completion record faulting features";
    string limpio3 = "Enable DSA 2.0 Event Log and completion record faulting features";
    tema3.limpiaAsunto.should == limpio3;

    string tema4 = "scsi: ipr: Remove SATA support";
    string limpio4 = "ipr: Remove SATA support";
    tema4.limpiaAsunto.should == limpio4;

    string tema5 = "[PATCH 0/2] MediaTek AUXADC thermal: urgent fixes";
    string limpio5 = "MediaTek AUXADC thermal: urgent fixes";
    tema5.limpiaAsunto.should == limpio5;
}

@("expresiones regulares")
unittest {
    import lei : exprMultiple;
    import std.regex : matchFirst;

    auto str1 = "[Intel-gfx] [PATCH v8 03/11] drm/i915/perf: Validate OA sseu config outside switch";
    auto m = matchFirst(str1, exprMultiple);
    m["version"].should == "8";
    m["serie"].should == "03";
    m["max"].should == "11";
    m["asunto"].should == "drm/i915/perf: Validate OA sseu config outside switch";

    auto str2 = "[PATCHv3 00/18] saa7146: convert to vb2";
    auto m2 = matchFirst(str2, exprMultiple);
    m2["version"].should == "3";
    m2["serie"].should == "00";
    m2["max"].should == "18";
    m2["asunto"].should == "saa7146: convert to vb2";
}

@("01-Doble")
unittest {
    string path = buildPath("pruebas", "01-doble");

    string[] log = logBuf();
    string[] asuntos = asuntosDeCorreos(path);

    Serie[] ser = seriesDeAsuntos(asuntos, log, gitRepo, sitio, path);

    Serie result = Serie([
        0: Tema("[PATCH v4 0/8] Export platform features from ccp driver"),
        1: Tema("[PATCH v4 1/8] crypto: ccp: Drop TEE support for IRQ handler", "a7ca7bbdb59e59a7890116e03a3bb99bcf4c87a6", true, gitRepo),
        2: Tema("[PATCH v4 2/8] crypto: ccp: Add a header for multiple drivers to use `__psp_pa`", "ae7d45fb7ca75e94b478e2404709ba3024774334", true, gitRepo),
        3: Tema("[PATCH v4 3/8] crypto: ccp: Move some PSP mailbox bit definitions into common header", "1c5c1daf04d13916867ef68c6ba7ae4f5e73801f", true, gitRepo),
        4: Tema("[PATCH v4 4/8] crypto: ccp: Add support for an interface for platform features", "7ccc4f4e2e50e4a29f9ee8f5c9e187f8491bb6e7", true, gitRepo),
        5: Tema("[PATCH v4 5/8] crypto: ccp: Enable platform access interface on client PSP parts", "22351239247b30978d06eb2ab5c258e6b344949f", true, gitRepo),
        6: Tema("[PATCH v4 6/8] i2c: designware: Use PCI PSP driver for communication", "440da737cf8d35a1b2205678cc1879fa90948f7a", true, gitRepo),
        7: Tema("[PATCH v4 7/8] crypto: ccp: Add support for ringing a platform doorbell", "d5812571f594b03438d0d7acd0dc044f73c1719e", true, gitRepo),
        8: Tema("[PATCH v4 8/8] i2c: designware: Add doorbell support for Skyrim")
    ]);

    result.nivel = 4;

    // Hay dos porque el commit tiene "crypto - ccp" y el correo "crypto: ccp"
    ser.length.should == 2;
    result.should == ser[0];
}

@("02-Único")
unittest {
    string path = buildPath("pruebas", "02-único");

    string[] log = logBuf();
    string[] asuntos = asuntosDeCorreos(path);

    Serie[] ser = seriesDeAsuntos(asuntos, log, gitRepo, sitio, path);

    Serie result = Serie([
        -1: Tema("[PATCH] fs/buffer.c: use b_folio for fsverity work", "8b7d3fe96881e0f13c0176812b40432558882023", true, gitRepo)
    ]);
    result.nivel = -1;

    result.should == ser[0];
}

@("03-conNet")
unittest {
    string path = buildPath("pruebas", "03-conNet");

    string[] log = logBuf();
    string[] asuntos = asuntosDeCorreos(path);

    Serie[] ser = seriesDeAsuntos(asuntos, log, gitRepo, sitio, path);

    Serie result = Serie([
        0: Tema("[PATCH net v3 0/4] net: phy: mscc: support VSC8501"),
        1: Tema("[PATCH net v3 1/4] net: phy: mscc: add VSC8502 to MODULE_DEVICE_TABLE"),
        2: Tema("[PATCH net v3 2/4] net: phy: mscc: add support for VSC8501"),
        3: Tema("[PATCH net v3 3/4] net: phy: mscc: remove unnecessary phydev locking"),
        4: Tema("[PATCH net v3 4/4] net: phy: mscc: enable VSC8501/2 RGMII RX clock"),
    ]);
    result.nivel = 3;

    ser.length.should == 1;
    result.should == ser[0];
}

@("04-múltiples números")
unittest {
    string path = buildPath("pruebas", "04-sinMúltiplesNúmeros");

    string[] log = logBuf();
    string[] asuntos = asuntosDeCorreos(path);

    Serie[] ser = seriesDeAsuntos(asuntos, log, gitRepo, sitio, path);

    Serie result = Serie([
        0:  Tema("[PATCH v4 00/16] Enable DSA 2.0 Event Log and completion record faulting features"),
        1:  Tema("[PATCH v4 01/16] dmaengine: idxd: make misc interrupt one shot", "0c40bfb4c2dfad00a15337bb6213f92a797e3695", true, gitRepo),
        2:  Tema("[PATCH v4 02/16] dmaengine: idxd: add event log size sysfs attribute", "1649091f9180470f96f001724a4902d5d82bbd75", true, gitRepo),
        3:  Tema("[PATCH v4 03/16] dmaengine: idxd: setup event log configuration", "244da66cda359227d80ccb41dbcb99da40eae186", true, gitRepo),
        4:  Tema("[PATCH v4 04/16] dmaengine: idxd: add interrupt handling for event log", "2f431ba908d2ef05da478d10925207728f1ff483", true, gitRepo),
        5:  Tema("[PATCH v4 05/16] dmanegine: idxd: add debugfs for event log dump", "5fbe6503b52f5665560584f62adab5db70ac910e", true, gitRepo),
        6:  Tema("[PATCH v4 06/16] dmaengine: idxd: add per DSA wq workqueue for processing cr faults", "2f30decd2f23a376d2ed73dfe4c601421edf501a", true, gitRepo),
        7:  Tema("[PATCH v4 07/16] dmaengine: idxd: create kmem cache for event log fault items", "c2f156bf168fb42cd6ecd0a8e2204dbe542b8516", true, gitRepo),
        8:  Tema("[PATCH v4 08/16] dmaengine: idxd: add idxd_copy_cr() to copy user completion record during page fault handling", "b022f59725f0ae846191abbd6d2e611d7f60f826", true, gitRepo),
        9:  Tema("[PATCH v4 09/16] dmaengine: idxd: process user page faults for completion record", "c40bd7d9737bdcfb02d42765bc6c59b338151123", true, gitRepo),
        10:  Tema("[PATCH v4 10/16] dmaengine: idxd: add descs_completed field for completion record", "6926987185a3ae92c31b99ce1bfdfb04e95057c0", true, gitRepo),
        11:  Tema("[PATCH v4 11/16] dmaengine: idxd: process batch descriptor completion record faults", "2442b7473ad03671378d2d95651bd6bbe09a0943", true, gitRepo),
        12:  Tema("[PATCH v4 12/16] dmaengine: idxd: add per file user counters for completion record faults", "fecae134ee10b7de69461c197450f7c05677e733", true, gitRepo),
        13: Tema("[PATCH v4 13/16] dmaengine: idxd: add a device to represent the file opened", "e6fd6d7e5f0fe4a17a08e892afb5db800e7794ec", true, gitRepo),
        14:  Tema("[PATCH v4 14/16] dmaengine: idxd: expose fault counters to sysfs", "244009b07e7d0728726f266cc3485d7fd400d0d5", true, gitRepo),
        15: Tema("[PATCH v4 15/16] dmaengine: idxd: add pid to exported sysfs attribute for opened file", "a62b8f87c770fa4109ce515e4d8a0d4701a4ca5f", true, gitRepo),
        16:  Tema("[PATCH v4 16/16] dmaengine: idxd: add per wq PRS disable", "f2dc327131b5cbb2cbb467cec23836f2e9d4cf46", true, gitRepo),
    ]);
    result.nivel = 4;

    ser.length.should == 1;
    ser[0].should == result;
}

@("05-Caractéres raros")
unittest {
    string path = buildPath("pruebas", "05-caractéresRaros");

    string[] log = logBuf();
    string[] asuntos = asuntosDeCorreos(path);

    Serie[] ser = seriesDeAsuntos(asuntos, log, gitRepo, sitio, path);

    Serie result = Serie([
        0: Tema("[PATCH v2 0/5] locking: Introduce local{,64}_try_cmpxchg"),
        1: Tema("[PATCH v2 1/5] locking/atomic: Add generic try_cmpxchg{,64}_local support"),
        2: Tema("[PATCH v2 2/5] locking/generic: Wire up local{,64}_try_cmpxchg"),
        3: Tema("[PATCH v2 3/5] locking/arch: Wire up local_try_cmpxchg"),
        4: Tema("[PATCH v2 4/5] locking/x86: Define arch_try_cmpxchg_local"),
        5: Tema("[PATCH v2 5/5] events: Illustrate the transition to local{,64}_try_cmpxchg"),
    ]);
    result.nivel = 2;
    result.sacaCommitsDeTemas(gitRepo, log);

    result.temasTienenCommit([2, 3, 4]).should == true;
    result.temas[0].commit.isNull.should == true;
    result.temas[1].commit.isNull.should == true;
    result.temas[2].commit.isNull.should == false;
    result.temas[3].commit.isNull.should == false;
    result.temas[4].commit.isNull.should == false;
    result.temas[5].commit.isNull.should == true;

    ser.length.should == 1;
    result.should == ser[0];
}

@("06-no commit")
unittest {
    string path = buildPath("pruebas", "06-noCommit");

    string[] log = logBuf();
    string[] asuntos = asuntosDeCorreos(path);

    Serie[] ser = seriesDeAsuntos(asuntos, log, gitRepo, sitio, path);

    Serie result = Serie([
        -1: Tema("scsi: ipr: Remove SATA support"),

    ]);
    result.sacaCommitsDeTemas(gitRepo, log);
    result.nivel = -1;

    ser.length.should == 1;
    result.should == ser[0];
    result.opEquals(ser[0], true).should == true;
}

@("07-Correos que faltan")
unittest {
    string path = buildPath("pruebas", "07-correosQueFaltan");

    string[] log = logBuf();
    string[] asuntos = asuntosDeCorreos(path);

    Serie[] ser = seriesDeAsuntos(asuntos, log, gitRepo, sitio, path);

    Serie result = Serie([
        1: Tema("[PATCH v4 01/13] rust: sync: introduce `LockClassKey`"),
        2: Tema("[PATCH v4 02/13] rust: sync: introduce `Lock` and `Guard`"),
        3: Tema("[PATCH v4 03/13] rust: lock: introduce `Mutex`"),
        4: Tema("[PATCH v4 04/13] locking/spinlock: introduce spin_lock_init_with_key"),
        5: Tema("[PATCH v4 05/13] rust: lock: introduce `SpinLock`"),
        6: Tema("[PATCH v4 06/13] rust: lock: add support for `Lock::lock_irqsave`"),
        7: Tema("[PATCH v4 07/13] rust: lock: implement `IrqSaveBackend` for `SpinLock`"),
        8: Tema("[PATCH v4 08/13] rust: introduce `ARef`"),
        9: Tema("[PATCH v4 09/13] rust: add basic `Task`"),
        10: Tema("[PATCH v4 10/13] rust: introduce `current`"),
        11: Tema("[PATCH v4 11/13] rust: lock: add `Guard::do_unlocked`"),
        12: Tema("[PATCH v4 12/13] rust: sync: introduce `CondVar`"),
        13: Tema("[PATCH v4 13/13] rust: sync: introduce `LockedBy`"),
    ]);
    result.sacaCommitsDeTemas(gitRepo, log);
    result.nivel = 4;

    result.temasTienenCommit([1, 2, 3, 5, 8, 9, 10, 11, 12, 13]).should == true;
    ser[0].temasTienenCommit([1, 2, 3, 5, 8, 9, 10, 11, 12, 13]).should == true;

    ser.length.should == 1;
    result.should == ser[0];
    result.opEquals(ser[0], true).should == true;
}

@("08-Commits que faltan")
unittest {
    string path = buildPath("pruebas", "08-faltanCommits");

    string[] log = logBuf();
    string[] asuntos = asuntosDeCorreos(path);

    Serie[] ser = seriesDeAsuntos(asuntos, log, gitRepo, sitio, path);

    Serie result = Serie([
        0: Tema("[PATCH 0/2] MediaTek AUXADC thermal: urgent fixes"),
        1: Tema("[PATCH 1/2] Revert \"thermal/drivers/mediatek: Add delay after thermal banks initialization\""),
        2: Tema("[PATCH 2/2] thermal/drivers/mediatek: Add temperature constraints to validate read"),
    ]);
    result.sacaCommitsDeTemas(gitRepo, log);
    result.nivel = -1;

    result.temasTienenCommit([1, 2]).should == true;
    ser[0].temasTienenCommit([1, 2]).should == true;

    ser.length.should == 1;
    result.should == ser[0];
    result.opEquals(ser[0], true).should == true;
}

@("09-Elimina candidatos estable")
unittest {
    string path = buildPath("pruebas", "09-eliminaCandidatosEstable");

    string[] log = logBuf();
    string[] asuntos = asuntosDeCorreos(path);

    Serie[] ser = seriesDeAsuntos(asuntos, log, gitRepo, sitio, path);

    Serie result = Serie([
        -1: Tema("fbdev: modedb: Add 1920x1080 at 60 Hz video mode"),
    ]);
    result.sacaCommitsDeTemas(gitRepo, log);
    result.nivel = -1;
    result.temasTienenCommit([-1]).should == true;
    ser[0].temasTienenCommit([-1]).should == true;

    ser.length.should == 1;
    result.should == ser[0];
}

@("10-Espacio extra en URL")
unittest {
    // El commit en este tiene como tema "loongarch" y no LoongArch. No sé
    // qué es lo de "espacio extra"
    string path = buildPath("pruebas", "10-espacioExtraEnLaURL");

    string[] log = logBuf();
    string[] asuntos = asuntosDeCorreos(path);

    Serie[] ser = seriesDeAsuntos(asuntos, log, gitRepo, sitio, path);

    Serie result = Serie([
        -1: Tema("[PATCH] LoongArch: Add support for function error injection"),
    ]);
    result.sacaCommitsDeTemas(gitRepo, log);
    result.nivel = -1;

    result.temasTienenCommit([-1]).should == true;
    ser[0].temasTienenCommit([-1]).should == true;

    ser.length.should == 1;
    result.should == ser[0];
    result.opEquals(ser[0], true).should == true;
}

@("11-Mala clasificación diferencias")
unittest {
    import mantenedores : ÁreaMantenida, Entrada;

    string path = buildPath("pruebas", "11-malaClasificaciónDiferencias");

    string[] log = logBuf();
    string[] asuntos = asuntosDeCorreos(path);


    Serie[] ser = seriesDeAsuntos(asuntos, log, gitRepo, sitio, path);

    Serie result = Serie([
         0: Tema("[PATCH v7 00/12] Add PCI pass-thru support to Hyper-V Confidential VMs"),
         1: Tema("[PATCH v7 01/12] x86/ioremap: Add hypervisor callback for private MMIO mapping in coco VM"),
         2: Tema("[PATCH v7 02/12] x86/hyperv: Reorder code to facilitate future work"),
         3: Tema("[PATCH v7 03/12] Drivers: hv: Explicitly request decrypted in vmap_pfn() calls"),
         4: Tema("[PATCH v7 04/12] x86/mm: Handle decryption/re-encryption of bss_decrypted consistently"),
         5: Tema("[PATCH v7 05/12] init: Call mem_encrypt_init() after Hyper-V hypercall init is done"),
         6: Tema("[PATCH v7 06/12] x86/hyperv: Change vTOM handling to use standard coco mechanisms"),
         7: Tema("[PATCH v7 07/12] swiotlb: Remove bounce buffer remapping for Hyper-V"),
         8: Tema("[PATCH v7 08/12] Drivers: hv: vmbus: Remove second mapping of VMBus monitor pages"),
         9: Tema("[PATCH v7 09/12] Drivers: hv: vmbus: Remove second way of mapping ring buffers"),
        10: Tema("[PATCH v7 10/12] hv_netvsc: Remove second mapping of send and recv buffers"),
        11: Tema("[PATCH v7 11/12] Drivers: hv: Don't remap addresses that are above shared_gpa_boundary"),
        12: Tema("[PATCH v7 12/12] PCI: hv: Enable PCI pass-thru devices in Confidential VMs"),
    ]);
    result.sacaCommitsDeTemas(gitRepo, log);
    result.nivel = 7;

    result.temasTienenCommit([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]).should == true;
    ser[0].temasTienenCommit([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]).should == true;

    ser.length.should == 1;

    ÁreaMantenida[] ret = sacarÁreasDeSerie(ser[0]);

    ret.should == [
        ÁreaMantenida("Hyper-V/Azure CORE AND DRIVERS", [Entrada('F', "drivers/pci/controller/pci-hyperv.c")]),
        ÁreaMantenida("Hyper-V/Azure CORE AND DRIVERS", [Entrada('F', "arch/x86/include/asm/hyperv-tlfs.h")]),
        ÁreaMantenida("Hyper-V/Azure CORE AND DRIVERS", [Entrada('F', "include/asm-generic/hyperv-tlfs.h")]),
        ÁreaMantenida("Hyper-V/Azure CORE AND DRIVERS", [Entrada('F', "arch/x86/include/asm/mshyperv.h")]),
        ÁreaMantenida("Hyper-V/Azure CORE AND DRIVERS", [Entrada('F', "include/asm-generic/mshyperv.h")]),
        ÁreaMantenida("Hyper-V/Azure CORE AND DRIVERS", [Entrada('F', "arch/x86/kernel/cpu/mshyperv.c")]),
        ÁreaMantenida("PCI NATIVE HOST BRIDGE AND ENDPOINT DRIVERS", [Entrada('F', "drivers/pci/controller/")]),
        ÁreaMantenida("DMA MAPPING HELPERS", [Entrada('F', "include/linux/swiotlb.h")]),
        ÁreaMantenida("GENERIC INCLUDE/ASM HEADER FILES", [Entrada('F', "include/asm-generic/")]),
        ÁreaMantenida("Hyper-V/Azure CORE AND DRIVERS", [Entrada('F', "drivers/net/hyperv/")]),
        ÁreaMantenida("Hyper-V/Azure CORE AND DRIVERS", [Entrada('F', "arch/x86/hyperv")]),
        ÁreaMantenida("PCI SUBSYSTEM", [Entrada('F', "drivers/pci/")]),
        ÁreaMantenida("X86 MM", [Entrada('F', "arch/x86/mm/")]),
        ÁreaMantenida("NETWORKING DRIVERS", [Entrada('F', "drivers/net/")]),
        ÁreaMantenida("Hyper-V/Azure CORE AND DRIVERS", [Entrada('F', "drivers/hv/")]),
        ÁreaMantenida("DMA MAPPING HELPERS", [Entrada('F', "kernel/dma/")]),
        ÁreaMantenida("X86 ARCHITECTURE (32-BIT AND 64-BIT)", [Entrada('F', "arch/x86/")])
    ];

    result.should == ser[0];
    result.opEquals(ser[0], true).should == true;
}

@("12-Correos duplicados")
unittest {
    import mantenedores : ÁreaMantenida, Entrada;

    string path = buildPath("pruebas", "12-correosDuplicados");

    string[] log = logBuf();
    string[] asuntos = asuntosDeCorreos(path);

    Serie[] ser = seriesDeAsuntos(asuntos, log, gitRepo, sitio, path);

    Serie result = Serie([
         0: Tema("[PATCH REBASE V3 0/4] sched/numa: Enhance vma scanning"),
         1: Tema("[PATCH REBASE V3 1/4] sched/numa: Apply the scan delay to every new vma"),
         2: Tema("[PATCH REBASE V3 2/4] sched/numa: Enhance vma scanning logic"),
         3: Tema("[PATCH REBASE V3 3/4] sched/numa: implement access PID reset logic"),
         4: Tema("[PATCH REBASE V3 4/4] sched/numa: Use hash_32 to mix up PIDs accessing VMA"),
    ]);
    result.sacaCommitsDeTemas(gitRepo, log);
    result.nivel = 3;

    ser[0].temasTienenCommit([1, 2, 3, 4]).should == true;

    ser.length.should == 1;

    ÁreaMantenida[] ret = sacarÁreasDeSerie(ser[0]);

    ret.should == [
        ÁreaMantenida("MEMORY MANAGEMENT", [Entrada('F', "include/linux/mm.h")]),
        ÁreaMantenida("SCHEDULER", [Entrada('F', "kernel/sched/")]),
        ÁreaMantenida("MEMORY MANAGEMENT", [Entrada('F', "mm/")]),
    ];
    result.should == ser[0];
    result.opEquals(ser[0], true).should == true;
}

@("13-sin tema")
unittest {
    import mantenedores : ÁreaMantenida, Entrada;

    string path = buildPath("pruebas", "13-sinTema");

    string[] log = logBuf();
    string[] asuntos = asuntosDeCorreos(path);

    Serie[] ser = seriesDeAsuntos(asuntos, log, gitRepo, sitio, path);

    Serie result = Serie([
        1: Tema("[PATCH v3 1/2] dt-bindings: clk: si521xx: Add Skyworks Si521xx I2C PCIe clock generators"),
        2: Tema("[PATCH v3 2/2] clk: si521xx: Clock driver for Skyworks Si521xx I2C PCIe clock generators"),
    ]);
    result.sacaCommitsDeTemas(gitRepo, log);
    result.nivel = 3;

    ser[0].temasTienenCommit([1, 2]).should == true;

    ser.length.should == 1;

    ÁreaMantenida[] ret = sacarÁreasDeSerie(ser[0]);

    ret.should == [
        ÁreaMantenida("COMMON CLK FRAMEWORK", [Entrada('F', "Documentation/devicetree/bindings/clock/")]),
        ÁreaMantenida("OPEN FIRMWARE AND FLATTENED DEVICE TREE BINDINGS", [Entrada('F', "Documentation/devicetree/")]),
        ÁreaMantenida("DOCUMENTATION", [Entrada('F', "Documentation/")]),
        ÁreaMantenida("COMMON CLK FRAMEWORK", [Entrada('F', "drivers/clk/")])
    ];

    result.should == ser[0];
    result.opEquals(ser[0], true).should == true;
}

@("14-sin commit-1")
unittest {
    import mantenedores : ÁreaMantenida, Entrada;

    string path = buildPath("pruebas", "14-sinCommit");

    string[] log = logBuf();
    string[] asuntos = asuntosDeCorreos(path);

    Serie[] ser = seriesDeAsuntos(asuntos, log, gitRepo, sitio, path);
    ser.length.should == 1;

    Serie result = Serie([
        -1: Tema("crypto: p10-aes-gcm - Supporting functions for ghash"),
    ]);
    result.sacaCommitsDeTemas(gitRepo, log);
    result.nivel = -1;
    result.should == ser[0];
    result.opEquals(ser[0], true).should == true;
    ser[0].temasTienenCommit([-1]).should == true;

    ÁreaMantenida[] ret = sacarÁreasDeSerie(ser[0]);
    ret.should == [
        ÁreaMantenida("IBM Power VMX Cryptographic instructions", [Entrada('F', "arch/powerpc/crypto/ghashp8-ppc.pl")]),
        ÁreaMantenida("LINUX FOR POWERPC (32-BIT AND 64-BIT)", [Entrada('F', "arch/powerpc/")]),
    ];
}

@("14-sin commit 2")
unittest {
    import mantenedores : ÁreaMantenida, Entrada;

    string path = buildPath("pruebas", "14-sinCommit-2");

    string[] log = logBuf();
    string[] asuntos = asuntosDeCorreos(path);

    Serie[] ser = seriesDeAsuntos(asuntos, log, gitRepo, sitio, path);
    ser.length.should == 1;

    Serie result = Serie([
        -1: Tema("[PATCH] regmap: Removed compressed cache support")
    ]);
    result.sacaCommitsDeTemas(gitRepo, log);
    result.nivel = -1;
    result.should == ser[0];
    result.opEquals(ser[0], true).should == true;
    ser[0].temasTienenCommit([-1]).should == true;

    ÁreaMantenida[] ret = sacarÁreasDeSerie(ser[0]);
    ret.should == [
        ÁreaMantenida("REGISTER MAP ABSTRACTION", [Entrada('F', "include/linux/regmap.h")]),
        ÁreaMantenida("REGISTER MAP ABSTRACTION", [Entrada('F', "drivers/base/regmap/")]),
        ÁreaMantenida("DRIVER CORE, KOBJECTS, DEBUGFS AND SYSFS", [Entrada('F', "drivers/base/")]),
    ];
}

@("14-sin commit 3")
unittest {
    import mantenedores : ÁreaMantenida, Entrada;

    string path = buildPath("pruebas", "14-sinCommit-3");

    string[] log = logBuf();
    string[] asuntos = asuntosDeCorreos(path);

    Serie[] ser = seriesDeAsuntos(asuntos, log, gitRepo, sitio, path);
    ser.length.should == 1;

    Serie result = Serie([
        -1: Tema("gpio: elkhartlake: Introduce Intel Elkhart Lake PSE GPIO")
    ]);
    result.sacaCommitsDeTemas(gitRepo, log);
    result.nivel = -1;
    result.should == ser[0];
    result.opEquals(ser[0], true).should == true;
    ser[0].temasTienenCommit([-1]).should == true;

    ÁreaMantenida[] ret = sacarÁreasDeSerie(ser[0]);
    ret.should == [
        ÁreaMantenida("INTEL GPIO DRIVERS", [Entrada('F', "drivers/gpio/gpio-elkhartlake.c")]),
        ÁreaMantenida("INTEL GPIO DRIVERS", [Entrada('F', "drivers/gpio/gpio-tangier.h")]),
        ÁreaMantenida("GPIO SUBSYSTEM", [Entrada('F', "drivers/gpio/")])
    ];
}

@("14-sin commit-4")
unittest {
    import mantenedores : ÁreaMantenida, Entrada;

    string path = buildPath("pruebas", "14-sinCommit-4");

    string[] log = logBuf();
    string[] asuntos = asuntosDeCorreos(path);

    Serie[] ser = seriesDeAsuntos(asuntos, log, gitRepo, sitio, path);
    ser.length.should == 1;

    Serie result = Serie([
        -1: Tema("[PATCH] hwmon: (nct6775) add Asus Pro A520M-C II/CSM"),
    ]);
    result.sacaCommitsDeTemas(gitRepo, log);
    result.nivel = -1;
    result.should == ser[0];
    result.opEquals(ser[0], true).should == true;
    ser[0].temasTienenCommit([-1]).should == true;

    ÁreaMantenida[] ret = sacarÁreasDeSerie(ser[0]);
    ret.should == [
        ÁreaMantenida("HARDWARE MONITORING", [Entrada('F', "drivers/hwmon/")])
    ];
}

@("14-sin commit-5")
unittest {
    import mantenedores : ÁreaMantenida, Entrada;

    string path = buildPath("pruebas", "14-sinCommit-5");

    string[] log = logBuf();
    string[] asuntos = asuntosDeCorreos(path);

    Serie[] ser = seriesDeAsuntos(asuntos, log, gitRepo, sitio, path);
    ser.length.should == 1;

    Serie result = Serie([
        0: Tema("[PATCH v13 0/1] media: i2c: imx334: support lower bandwidth mode"),
        1: Tema("[PATCH v13 1/1] media: i2c: imx334: update pixel, hblank and link frequency")
    ]);
    result.sacaCommitsDeTemas(gitRepo, log);
    result.nivel = 13;
    result.should == ser[0];
    result.opEquals(ser[0], true).should == true;
    ser[0].temasTienenCommit([0, 1]).should == true;

    ÁreaMantenida[] ret = sacarÁreasDeSerie(ser[0]);
    ret.should == [
        ÁreaMantenida("SONY IMX334 SENSOR DRIVER", [Entrada('F', "drivers/media/i2c/imx334.c")]),
        ÁreaMantenida("MEDIA INPUT INFRASTRUCTURE (V4L/DVB)", [Entrada('F', "drivers/media/")])
    ];
}

@("14-sin commit-6")
unittest {
    import mantenedores : ÁreaMantenida, Entrada;

    string path = buildPath("pruebas", "14-sinCommit-6");

    string[] log = logBuf();
    string[] asuntos = asuntosDeCorreos(path);

    Serie[] ser = seriesDeAsuntos(asuntos, log, gitRepo, sitio, path);
    ser.length.should == 1;

    Serie result6 = Serie([
        -1: Tema("Input: add a new Novatek NVT-ts driver"),
    ]);
    result6.sacaCommitsDeTemas(gitRepo, log);
    result6.nivel = -1;
    result6.should == ser[0];
    result6.opEquals(ser[0], true).should == true;
    ser[0].temasTienenCommit([-1]).should == true;

    ÁreaMantenida[] ret = sacarÁreasDeSerie(ser[0]);
    ret.should == [
       ÁreaMantenida("NOVATEK NVT-TS I2C TOUCHSCREEN DRIVER", [Entrada('F', "drivers/input/touchscreen/novatek-nvt-ts.c")]),
       ÁreaMantenida("INPUT (KEYBOARD, MOUSE, JOYSTICK, TOUCHSCREEN) DRIVERS", [Entrada('F', "drivers/input/")])
    ];
}

@("14-sin commit-7")
unittest {
    import mantenedores : ÁreaMantenida, Entrada;

    string path = buildPath("pruebas", "14-sinCommit-7");

    string[] log = logBuf();
    string[] asuntos = asuntosDeCorreos(path);

    Serie[] ser = seriesDeAsuntos(asuntos, log, gitRepo, sitio, path);
    ser.length.should == 1;

    Serie result = Serie([
        0: Tema("[PATCH v2] net: mana: Add new MANA VF performance counters for easier troubleshooting"),
    ]);
    result.sacaCommitsDeTemas(gitRepo, log);
    result.nivel = 2;
    result.should == ser[0];
    result.opEquals(ser[0], true).should == true;
    ser[0].temasTienenCommit([0]).should == true;

    ÁreaMantenida[] ret = sacarÁreasDeSerie(ser[0]);
    ret.should == [
        ÁreaMantenida("Hyper-V/Azure CORE AND DRIVERS", [Entrada('F', "drivers/net/ethernet/microsoft/")]),
        ÁreaMantenida("Hyper-V/Azure CORE AND DRIVERS", [Entrada('F', "include/net/mana")]),
        ÁreaMantenida("MICROSOFT MANA RDMA DRIVER", [Entrada('F', "include/net/mana")]),
        ÁreaMantenida("NETWORKING DRIVERS", [Entrada('F', "drivers/net/")]),
        ÁreaMantenida("NETWORKING [GENERAL]", [Entrada('F', "include/net/")])
    ];
}

@("15-demasiados temas")
unittest {
    import mantenedores : ÁreaMantenida, Entrada;

    string path = buildPath("pruebas", "15-demasiadosTemas");

    string[] log = logBuf();
    string[] asuntos = asuntosDeCorreos(path);

    Serie[] ser = seriesDeAsuntos(asuntos, log, gitRepo, sitio, path);
    ser.length.should == 1;

    Serie result = Serie([
        0 : Tema("[Intel-gfx] [PATCH v8 00/11] Add OAM support for MTL"),
        1 : Tema("[Intel-gfx] [PATCH v8 01/11] drm/i915/perf: Drop wakeref on GuC RC error"),
        2 : Tema("[Intel-gfx] [PATCH v8 02/11] drm/i915/mtl: Synchronize i915/BIOS on C6 enabling"),
        3 : Tema("[Intel-gfx] [PATCH v8 03/11] drm/i915/perf: Validate OA sseu config outside switch"),
        4 : Tema("[Intel-gfx] [PATCH v8 04/11] drm/i915/perf: Group engines into respective OA groups"),
        5 : Tema("[Intel-gfx] [PATCH v8 05/11] drm/i915/perf: Fail modprobe if i915_perf_init fails on OOM"),
        6 : Tema("[Intel-gfx] [PATCH v8 06/11] drm/i915/perf: Parse 64bit report header formats correctly"),
        7 : Tema("[Intel-gfx] [PATCH v8 07/11] drm/i915/perf: Handle non-power-of-2 reports"),
        8 : Tema("[Intel-gfx] [PATCH v8 08/11] drm/i915/perf: Add engine class instance parameters to perf"),
        9 : Tema("[Intel-gfx] [PATCH v8 09/11] drm/i915/perf: Add support for OA media units"),
        10 : Tema("[Intel-gfx] [PATCH v8 10/11] drm/i915/perf: Pass i915 object to perf revision helper"),
        11 : Tema("[Intel-gfx] [PATCH v8 11/11] drm/i915/perf: Wa_14017512683: Disable OAM if media C6 is enabled in BIOS"),
    ]);

    result.sacaCommitsDeTemas(gitRepo, log);
    result.nivel = 8;
    result.should == ser[0];
    result.opEquals(ser[0], true).should == true;
    ser[0].temasTienenCommit([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]).should == true;

    ÁreaMantenida[] ret = sacarÁreasDeSerie(ser[0]);
    ret.should == [
        ÁreaMantenida("INTEL DRM I915 DRIVER (Meteor Lake, DG2 and older excluding Poulsbo, Moorestown and derivative)", [Entrada('F', "include/uapi/drm/i915_drm.h")]),
        ÁreaMantenida("INTEL DRM I915 DRIVER (Meteor Lake, DG2 and older excluding Poulsbo, Moorestown and derivative)", [Entrada('F', "drivers/gpu/drm/i915/")]),
        ÁreaMantenida("DRM DRIVERS", [Entrada('F', "include/uapi/drm/")]),
        ÁreaMantenida("DRM DRIVERS AND MISC GPU PATCHES", [Entrada('F', "include/uapi/drm/")]),
        ÁreaMantenida("DRM DRIVERS AND MISC GPU PATCHES", [Entrada('F', "drivers/gpu/drm/")]),
        ÁreaMantenida("DRM DRIVERS", [Entrada('F', "drivers/gpu/")])
    ];
}

@("16-tema cero demasiadas veces-0")
unittest {
    import mantenedores : ÁreaMantenida, Entrada;

    string path = buildPath("pruebas", "16-temaCeroVariasVeces");

    string[] log = logBuf();
    string[] asuntos = asuntosDeCorreos(path);

    Serie[] ser = seriesDeAsuntos(asuntos, log, gitRepo, sitio, path);
    ser.length.should == 1;

    Serie result = Serie([
        0: Tema("[PATCH v5 0/3] Add EMAC3 support for sa8540p-ride (devicetree/clk bits)"),
        1: Tema("[PATCH v5 1/3] clk: qcom: gcc-sc8280xp: Add EMAC GDSCs"),
        2: Tema("[PATCH v5 2/3] arm64: dts: qcom: sc8280xp: Add ethernet nodes"),
        3: Tema("[PATCH v5 3/3] arm64: dts: qcom: sa8540p-ride: Add ethernet nodes"),
    ]);

    result.nivel = 5;
    result.should == ser[0];
    result.opEquals(ser[0], true).should == true;
    ser[0].temasTienenCommit([1, 2, 3]).should == true;

    ÁreaMantenida[] ret = sacarÁreasDeSerie(ser[0]);
    ret.should == [
        ÁreaMantenida("COMMON CLK FRAMEWORK", [Entrada('F', "include/dt-bindings/clock/")]),
        ÁreaMantenida("ARM/QUALCOMM MAILING LIST", [Entrada('F', "arch/arm64/boot/dts/qcom/")]),
        ÁreaMantenida("ARM/QUALCOMM SUPPORT", [Entrada('F', "arch/arm64/boot/dts/qcom/")]),
        ÁreaMantenida("OPEN FIRMWARE AND FLATTENED DEVICE TREE BINDINGS", [Entrada('F', "include/dt-bindings/")]),
        ÁreaMantenida("QUALCOMM CLOCK DRIVERS", [Entrada('F', "drivers/clk/qcom/")]),
        ÁreaMantenida("COMMON CLK FRAMEWORK", [Entrada('F', "drivers/clk/")]),
        ÁreaMantenida("ARM64 PORT (AARCH64 ARCHITECTURE)", [Entrada('F', "arch/arm64/")])
    ];
}

@("16-tema cero demasiadas veces-1")
unittest {
    import mantenedores : ÁreaMantenida, Entrada;

    string path = buildPath("pruebas", "16-temaCeroVariasVeces-01");

    string[] log = logBuf();
    string[] asuntos = asuntosDeCorreos(path);

    Serie[] ser = seriesDeAsuntos(asuntos, log, gitRepo, sitio, path);
    ser.length.should == 1;

    Serie result = Serie([
         0: Tema("[PATCHv3 00/18] saa7146: convert to vb2"),
         1: Tema("[PATCHv3 01/18] media: common: saa7146: disable clipping", "common: saa7146: disable clipping"),
         2: Tema("[PATCHv3 02/18] common/saa7146: fix VFL direction for vbi output"),
         3: Tema("[PATCHv3 03/18] media: pci: saa7146: hexium_orion: initialize input 0"),
         4: Tema("[PATCHv3 04/18] media: saa7146: drop 'dev' and 'resources' from struct saa7146_fh"),
         5: Tema("[PATCHv3 05/18] media: common: saa7146: drop 'fmt' from struct saa7146_buf"),
         6: Tema("[PATCHv3 06/18] media: common: saa7146: replace BUG_ON by WARN_ON"),
         7: Tema("[PATCHv3 07/18] staging: media: av7110: replace BUG_ON by WARN_ON"),
         8: Tema("[PATCHv3 08/18] media: common: saa7146: fix broken V4L2_PIX_FMT_YUV422P support"),
         9: Tema("[PATCHv3 09/18] media: common: saa7146: use for_each_sg_dma_page"),
        10: Tema("[PATCHv3 10/18] media: saa7146: convert to vb2", "saa7146: convert to vb2"),
        11: Tema("[PATCHv3 11/18] media: common: saa7146: fix compliance problems with field handling"),
        12: Tema("[PATCHv3 12/18] media: common: saa7146: check minimum video format size"),
        13: Tema("[PATCHv3 13/18] media: common: saa7146: allow S_STD(G_STD)"),
        14: Tema("[PATCHv3 14/18] media: mxb: update the tvnorms when changing input"),
        15: Tema("[PATCHv3 15/18] media: common: saa7146: add support for missing .vidioc_try_fmt_vbi_cap"),
        16: Tema("[PATCHv3 16/18] media: mxb: allow tuner/input/audio ioctls for vbi"),
        17: Tema("[PATCHv3 17/18] media: pci: saa7146: advertise only those TV standard that are supported"),
        18: Tema("[PATCHv3 18/18] staging: media: av7110: fix VBI output support"),
    ]);

    result.sacaCommitsDeTemas(gitRepo, log);
    result.nivel = 3;
    result.comparaPrint(ser[0]).shouldBeTrue;

    ser[0].temasTienenCommit([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18]).should == true;

    ÁreaMantenida[] ret = sacarÁreasDeSerie(ser[0]);
    ret.should == [
        ÁreaMantenida("SAA7146 VIDEO4LINUX-2 DRIVER", [Entrada('F', "drivers/media/common/saa7146/")]),
        ÁreaMantenida("SAA7146 VIDEO4LINUX-2 DRIVER", [Entrada('F', "drivers/media/pci/saa7146/")]),
        ÁreaMantenida("MEDIA INPUT INFRASTRUCTURE (V4L/DVB)", [Entrada('F', "drivers/staging/media/")]),
        ÁreaMantenida("STAGING SUBSYSTEM", [Entrada('F', "drivers/staging/")]),
        ÁreaMantenida("MEDIA INPUT INFRASTRUCTURE (V4L/DVB)", [Entrada('F', "drivers/media/")]),
        ÁreaMantenida("MEDIA INPUT INFRASTRUCTURE (V4L/DVB)", [Entrada('F', "include/media/")])
    ];
}

@("17-sin tema-1")
unittest {
    import mantenedores : ÁreaMantenida, Entrada;

    string path = buildPath("pruebas", "17-sinTema-1");

    string[] log = logBuf();
    string[] asuntos = asuntosDeCorreos(path);

    Serie[] ser = seriesDeAsuntos(asuntos, log, gitRepo, sitio, path);
    ser.length.should == 1;

    Serie result = Serie([
        1: Tema("[PATCH v3 1/3] dt-bindings: mfd: x-powers,axp152: Document the AXP15060 variant"),
        2: Tema("[PATCH v3 2/3] mfd: axp20x: Add support for AXP15060 PMIC"),
        3: Tema("[PATCH v3 3/3] regulator: axp20x: Add AXP15060 support"),
    ]);

    result.sacaCommitsDeTemas(gitRepo, log);
    result.nivel = 3;
    result.should == ser[0];
    result.opEquals(ser[0], true).should == true;
    ser[0].temasTienenCommit([1, 2, 3]).should == true;

    ÁreaMantenida[] ret = sacarÁreasDeSerie(ser[0]);
    ret.should == [
        ÁreaMantenida("MULTIFUNCTION DEVICES (MFD)", [Entrada('F', "Documentation/devicetree/bindings/mfd/")]),
        ÁreaMantenida("OPEN FIRMWARE AND FLATTENED DEVICE TREE BINDINGS", [Entrada('F', "Documentation/devicetree/")]),
        ÁreaMantenida("VOLTAGE AND CURRENT REGULATOR FRAMEWORK", [Entrada('F', "drivers/regulator/")]),
        ÁreaMantenida("MULTIFUNCTION DEVICES (MFD)", [Entrada('F', "include/linux/mfd/")]),
        ÁreaMantenida("DOCUMENTATION", [Entrada('F', "Documentation/")]),
        ÁreaMantenida("MULTIFUNCTION DEVICES (MFD)", [Entrada('F', "drivers/mfd/")])
    ];
}

@("17-sin tema-2")
unittest {
    import mantenedores : ÁreaMantenida, Entrada;

    string path = buildPath("pruebas", "17-sinTema-2");

    string[] log = logBuf();
    string[] asuntos = asuntosDeCorreos(path);

    Serie[] ser = seriesDeAsuntos(asuntos, log, gitRepo, sitio, path);
    ser.length.should == 1;

    Serie result = Serie([
        1: Tema("[PATCH v12 1/2] dt-bindings: mfd: Add MAX5970 and MAX5978"),
        2: Tema("[PATCH v12 2/2] mfd: max597x: Add support for MAX5970 and MAX5978"),
    ]);

    result.sacaCommitsDeTemas(gitRepo, log);
    result.nivel = 12;
    result.should == ser[0];
    result.opEquals(ser[0], true).should == true;
    ser[0].temasTienenCommit([1, 2]).should == true;

    ÁreaMantenida[] ret = sacarÁreasDeSerie(ser[0]);
    ret.should == [
        ÁreaMantenida("MULTIFUNCTION DEVICES (MFD)", [Entrada('F', "Documentation/devicetree/bindings/mfd/")]),
        ÁreaMantenida("OPEN FIRMWARE AND FLATTENED DEVICE TREE BINDINGS", [Entrada('F', "Documentation/devicetree/")]),
        ÁreaMantenida("MULTIFUNCTION DEVICES (MFD)", [Entrada('F', "include/linux/mfd/")]),
        ÁreaMantenida("DOCUMENTATION", [Entrada('F', "Documentation/")]),
        ÁreaMantenida("MULTIFUNCTION DEVICES (MFD)", [Entrada('F', "drivers/mfd/")])
    ];
}

@("17-sin tema-3")
unittest {
    import mantenedores : ÁreaMantenida, Entrada;

    string path = buildPath("pruebas", "17-sinTema-3");

    string[] log = logBuf();
    string[] asuntos = asuntosDeCorreos(path);

    Serie[] ser = seriesDeAsuntos(asuntos, log, gitRepo, sitio, path);
    ser.length.should == 1;

    Serie result = Serie([
        1: Tema("[PATCH v1 1/2] pinctrl: Remove Intel Thunder Bay pinctrl driver"),
        2: Tema("[PATCH v1 2/2] dt-bindings: pinctrl: Remove bindings for Intel Thunderbay pinctrl driver")
    ]);

    result.sacaCommitsDeTemas(gitRepo, log);
    result.nivel = 1;
    result.should == ser[0];
    result.opEquals(ser[0], true).should == true;
    ser[0].temasTienenCommit([1, 2]).should == true;

    ÁreaMantenida[] ret = sacarÁreasDeSerie(ser[0]);
    ret.should == [
        ÁreaMantenida("PIN CONTROL SUBSYSTEM", [Entrada('F', "Documentation/devicetree/bindings/pinctrl/")]),
        ÁreaMantenida("OPEN FIRMWARE AND FLATTENED DEVICE TREE BINDINGS", [Entrada('F', "Documentation/devicetree/")]),
        ÁreaMantenida("PIN CONTROL SUBSYSTEM", [Entrada('F', "drivers/pinctrl/")]),
        ÁreaMantenida("DOCUMENTATION", [Entrada('F', "Documentation/")])
    ];
}

@("17-sin tema-4")
unittest {
    import mantenedores : ÁreaMantenida, Entrada;

    string path = buildPath("pruebas", "17-sinTema-4");

    string[] log = logBuf();
    string[] asuntos = asuntosDeCorreos(path);

    Serie[] ser = seriesDeAsuntos(asuntos, log, gitRepo, sitio, path);
    ser.length.should == 1;

    Serie result = Serie([
        1: Tema("[PATCH v3 1/2] dt-bindings: regulator: Add Richtek RT5739"),
        2: Tema("[PATCH v3 2/2] regulator: Add support for Richtek RT5739 voltage regulator"),
    ]);

    result.sacaCommitsDeTemas(gitRepo, log);
    result.nivel = 3;
    result.should == ser[0];
    result.opEquals(ser[0], true).should == true;
    ser[0].temasTienenCommit([1, 2]).should == true;

    ÁreaMantenida[] ret = sacarÁreasDeSerie(ser[0]);
    ret.should == [
        ÁreaMantenida("VOLTAGE AND CURRENT REGULATOR FRAMEWORK", [Entrada('F', "Documentation/devicetree/bindings/regulator/")]),
        ÁreaMantenida("OPEN FIRMWARE AND FLATTENED DEVICE TREE BINDINGS", [Entrada('F', "Documentation/devicetree/")]),
        ÁreaMantenida("VOLTAGE AND CURRENT REGULATOR FRAMEWORK", [Entrada('F', "drivers/regulator/")]),
        ÁreaMantenida("DOCUMENTATION", [Entrada('F', "Documentation/")])
    ];
}

@("17-sin tema-5")
unittest {
    import mantenedores : ÁreaMantenida, Entrada;

    string path = buildPath("pruebas", "17-sinTema-5");

    string[] log = logBuf();
    string[] asuntos = asuntosDeCorreos(path);

    Serie[] ser = seriesDeAsuntos(asuntos, log, gitRepo, sitio, path);
    ser.length.should == 1;

    Serie result = Serie([
        1: Tema("[PATCH v4 1/2] usb: dwc3: core: add support for disabling High-speed park mode"),
        2: Tema("[PATCH v4 2/2] dt-bindings: usb: snps,dwc3: Add 'snps,parkmode-disable-hs-quirk' quirk"),
    ]);

    result.sacaCommitsDeTemas(gitRepo, log);
    result.nivel = 4;
    result.should == ser[0];
    result.opEquals(ser[0], true).should == true;
    ser[0].temasTienenCommit([1, 2]).should == true;

    ÁreaMantenida[] ret = sacarÁreasDeSerie(ser[0]);
    ret.should == [
        ÁreaMantenida("USB SUBSYSTEM", [Entrada('F', "Documentation/devicetree/bindings/usb/")]),
        ÁreaMantenida("OPEN FIRMWARE AND FLATTENED DEVICE TREE BINDINGS", [Entrada('F', "Documentation/devicetree/")]),
        ÁreaMantenida("DESIGNWARE USB3 DRD IP DRIVER", [Entrada('F', "drivers/usb/dwc3/")]),
        ÁreaMantenida("DOCUMENTATION", [Entrada('F', "Documentation/")]),
        ÁreaMantenida("USB SUBSYSTEM", [Entrada('F', "drivers/usb/")])
    ];
}

@("18-mala sección")
unittest {
    import mantenedores : ÁreaMantenida, Entrada;

    string path = buildPath("pruebas", "18-malaSección");

    string[] log = logBuf();
    string[] asuntos = asuntosDeCorreos(path);

    Serie[] ser = seriesDeAsuntos(asuntos, log, gitRepo, sitio, path);
    ser.length.should == 1;

    Serie result = Serie([
         0: Tema("[PATCH net-next 00/10] Improve IPsec limits, ESN and replay window in mlx5"),
         1: Tema("[PATCH net-next 01/10] net/mlx5e: Factor out IPsec ASO update function"),
         2: Tema("[PATCH net-next 02/10] net/mlx5e: Prevent zero IPsec soft/hard limits"),
         3: Tema("[PATCH net-next 03/10] net/mlx5e: Add SW implementation to support IPsec 64 bit soft and hard limits"),
         4: Tema("[PATCH net-next 04/10] net/mlx5e: Overcome slow response for first IPsec ASO WQE"),
         5: Tema("[PATCH net-next 05/10] xfrm: don't require advance ESN callback for packet offload"),
         6: Tema("[PATCH net-next 06/10] net/mlx5e: Remove ESN callbacks if it is not supported"),
         7: Tema("[PATCH net-next 07/10] net/mlx5e: Set IPsec replay sequence numbers"),
         8: Tema("[PATCH net-next 08/10] net/mlx5e: Reduce contention in IPsec workqueue"),
         9: Tema("[PATCH net-next 09/10] net/mlx5e: Generalize IPsec work structs"),
        10: Tema("[PATCH net-next 10/10] net/mlx5e: Simulate missing IPsec TX limits hardware functionality"),
    ]);

    result.sacaCommitsDeTemas(gitRepo, log);
    result.nivel = -1;
    result.should == ser[0];
    result.opEquals(ser[0], true).should == true;
    ser[0].temasTienenCommit([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]).should == true;

    ÁreaMantenida[] ret = sacarÁreasDeSerie(ser[0]);
    ret.should == [
        ÁreaMantenida("MELLANOX MLX5 core VPI driver", [Entrada('F', "drivers/net/ethernet/mellanox/mlx5/core/")]),
        ÁreaMantenida("NETWORKING DRIVERS", [Entrada('F', "drivers/net/")]),
        ÁreaMantenida("NETWORKING [IPSEC]", [Entrada('F', "net/xfrm/")]),
        ÁreaMantenida("NETWORKING [GENERAL]", [Entrada('F', "net/")])
    ];
}

@("19-mal título")
unittest {
    import mantenedores : ÁreaMantenida, Entrada;

    string path = buildPath("pruebas", "19-malTítulo");

    string[] log = logBuf();
    string[] asuntos = asuntosDeCorreos(path);

    Serie[] ser = seriesDeAsuntos(asuntos, log, gitRepo, sitio, path);
    ser.length.should == 1;

    Serie result = Serie([
        0: Tema("[PATCH v6 0/2] Support Nvidia BlueField-3 GPIO driver and pin controller"),
        1: Tema("[PATCH v6 1/2] gpio: mlxbf3: Add gpio driver support", "mlxbf3: Add gpio driver support"),
        2: Tema("[PATCH v6 2/2] pinctrl: mlxbf3: Add pinctrl driver support"),
     ]);

    result.sacaCommitsDeTemas(gitRepo, log);
    result.nivel = 6;
    result.should == ser[0];
    result.opEquals(ser[0], true).should == true;
    ser[0].temasTienenCommit([1, 2]).should == true;

    ÁreaMantenida[] ret = sacarÁreasDeSerie(ser[0]);
    ret.should == [
        ÁreaMantenida("PIN CONTROL SUBSYSTEM", [Entrada('F', "drivers/pinctrl/")]),
        ÁreaMantenida("GPIO SUBSYSTEM", [Entrada('F', "drivers/gpio/")])
    ];
}

@("20-Falta Asunto Cero")
unittest {
    import mantenedores : ÁreaMantenida, Entrada;

    string path = buildPath("pruebas", "20-faltaAsuntoCero");

    string[] log = logBuf();
    string[] asuntos = asuntosDeCorreos(path);

    Serie[] ser = seriesDeAsuntos(asuntos, log, gitRepo, sitio, path);
    ser.length.should == 1;

    ser[0].temasTienenCommit([1, 2, 3, 4]).should == true;
    ser[0].toString.should == " * SIN TEMA: <drm/amd/pm: add smu_13_0_6 mca dump support> " ~
         "<drm/amd/pm: update smu_v13_0_6 ppsmc header> <drm/amdgpu: add amdgpu mca debug sysfs support> " ~
         "<drm/amdgpu: add amdgpu smu mca dump feature support> " ~
         "[[https://git.kernel.org/linus/25396684b57f7d16306ca149c545db60b2d08dda|commit]], [[https://git.kernel.org/linus/bcd8dc49c0b969b781dd3af5ee8c9896a9315c5e|commit]], " ~
         "[[https://git.kernel.org/linus/4051844c6616c0bf33dd16342967ea5b931746ae|commit]], [[https://git.kernel.org/linus/7ff607e27233861b3f83e658317b3fb18b047229|commit]]";
}

@("21-sinTítulo")
unittest {
    import mantenedores : ÁreaMantenida, Entrada;

    string path = buildPath("pruebas", "21-sinTítulo");

    string[] log = logBuf();
    string[] asuntos = asuntosDeCorreos(path);

    Serie[] ser = seriesDeAsuntos(asuntos, log, gitRepo, sitio, path);
    ser.length.should == 1;

    ser[0].temasTienenCommit([0, 2, 3, 4]).should == true;
    ser[0].toString.should == " * NULOS: <crypto: hisilicon/zip - Remove driver>Remove zlib-deflate [[https://git.kernel.org/linus/30febae71c6182e0762dc7744737012b4f8e6a6d|commit]], [[https://git.kernel.org/linus/62a465c25e99b9a98259a6b7f5bb759f5296d501|commit]], [[https://git.kernel.org/linus/e9dd20e0e5f62d01d9404db2cf9824d1faebcf71|commit]]";
}

@("22-sinCommits")
unittest {
    import mantenedores : ÁreaMantenida, Entrada;

    string path = buildPath("pruebas", "22-sinCommits");

    string[] log = logBuf();
    string[] asuntos = asuntosDeCorreos(path);

    Serie[] ser = seriesDeAsuntos(asuntos, log, gitRepo, sitio, path);
    ser.length.should == 1;

    ser[0].temasTienenCommit([3]).should == true;
    ser[0].toString.should == " * NULOS: <selinux: shrink conditional avtab node array> <selinux: use arrays for avtab hashtable nodes>Avtab hotspot optimizations [[https://git.kernel.org/linus/9d140885e35dac6dff2c56eccacc13f4fc96188a|commit]]";
}

@("23-dosPuntosConEspacios")
unittest {
    string[] log = logBuf();

    Serie result = Serie([
        0: Tema("[HID Patchsets for Samsung driver v4 0/6] Patchsets for Samsung driver"),
        1: Tema("[HID Patchsets for Samsung driver v4 1/6] HID Samsung : Broaden device compatibility in samsung driver.", "a7ca7bbdb59e59a7890116e03a3bb99bcf4c87a6", true, gitRepo),
        2: Tema("[HID Patchsets for Samsung driver v4 2/6] HID: Samsung : Fix the checkpatch complain. Rewritten code using memcmp where applicable.", "ae7d45fb7ca75e94b478e2404709ba3024774334", true, gitRepo),
        3: Tema("[HID Patchsets for Samsung driver v4 3/6] HID: Samsung : Add Samsung wireless keyboard support.", "1c5c1daf04d13916867ef68c6ba7ae4f5e73801f", true, gitRepo),
        4: Tema("[HID Patchsets for Samsung driver v4 4/6] HID: Samsung : Add Samsung wireless gamepad support.", "7ccc4f4e2e50e4a29f9ee8f5c9e187f8491bb6e7", true, gitRepo),
        5: Tema("[HID Patchsets for Samsung driver v4 5/6] HID: Samsung : Add Samsung wireless action mouse support.", "22351239247b30978d06eb2ab5c258e6b344949f", true, gitRepo),
        6: Tema("[HID Patchsets for Samsung driver v4 6/6] HID: Samsung : Add Samsung wireless bookcover and universal keyboard support.", "440da737cf8d35a1b2205678cc1879fa90948f7a", true, gitRepo),
    ]);

    result.sacaCommitsDeTemas(gitRepo, log);
    result.temasTienenCommit([1, 3, 4, 5, 6]).shouldBeTrue;
    result.toString.should == " * NULOS: <HID: Samsung : Fix the checkpatch complain. Rewritten code using memcmp where applicable.>Patchsets for samsung driver [[https://git.kernel.org/linus/9b8e4adad160f5d084f28eb6075dadd1efe80c65|commit]], [[https://git.kernel.org/linus/ac203cdbe97a197dddd2925c542ad5a52a8693fa|commit]], [[https://git.kernel.org/linus/e7290046728627e8c7a8f49632581437ab6d4300|commit]], [[https://git.kernel.org/linus/46e779b087f6061d6453f3b263cc8602b407b6d1|commit]], [[https://git.kernel.org/linus/944536c2a4de9dadd39bda5e8e4f2617df8c3b57|commit]]";
}

@("24-sinCommit-8")
unittest {
    string[] log = logBuf();

    Serie result = Serie([
        0: Tema("exfat: add ioctls for accessing attributes", "0ab8ba71868594acfc717b8c7d1738b9118118ec", true, gitRepo),
    ]);

    result.sacaCommitsDeTemas(gitRepo, log);
    result.temasTienenCommit([0]).shouldBeTrue;
    result.toString.should == " * Add ioctls for accessing attributes [[https://git.kernel.org/linus/0ab8ba71868594acfc717b8c7d1738b9118118ec|commit]]";
}

@("24-serieSinVersion")
unittest {
    import mantenedores : ÁreaMantenida, Entrada;

    string path = buildPath("pruebas", "24-serieSinVersion");

    string[] log = logBuf();
    string[] asuntos = asuntosDeCorreos(path);

    Serie[] ser = seriesDeAsuntos(asuntos, log, gitRepo, sitio, path);
    ser.length.should == 1;

    ser[0].temasTienenCommit([1, 2, 3, 4, 5, 6, 7, 8]).should == true;
    ser[0].toString.should == " * Add io_uring futex/futexv support [[https://git.kernel.org/linus/5177c0cb306a8628bafbf1e6b7aa7e1b7436b8dc|commit]], [[https://git.kernel.org/linus/e9a56c9325ef28d5481712e85dd5d3f8b2a68e88|commit]], [[https://git.kernel.org/linus/12a4be50aff30ee8f2c6a64020c82a4e997e8d6c|commit]], [[https://git.kernel.org/linus/e52c43403c9b839a30a9cfc4b75109581389d764|commit]], [[https://git.kernel.org/linus/3b0781595431acafe3db6596e12deb46975d91dd|commit]], [[https://git.kernel.org/linus/8f350194d5cfd7016d4cd44e433df0faa4d4a703|commit]], [[https://git.kernel.org/linus/8af1692616d993c93a080865a7f19506733aa462|commit]], [[https://git.kernel.org/linus/194bb58c6090e39bd7d9b9c888a079213628e1f6|commit]]";
}

@("25-temaÚnicoMalClasificado")
unittest {
    import mantenedores : ÁreaMantenida, Entrada;

    string path = buildPath("pruebas", "25-temaÚnicoMalClasificado");

    string[] log = logBuf();
    string[] asuntos = asuntosDeCorreos(path);

    Serie[] ser = seriesDeAsuntos(asuntos, log, gitRepo, sitio, path);
    ser.length.should == 1;

    ser[0].temasTienenCommit([1]).should == true;
    ser[0].toString.should == " * (nct6683) add another customer id for asrock x670e taichi [[https://git.kernel.org/linus/05b68e18ec64663be11119dd48de52fec927dfde|commit]]";
}
