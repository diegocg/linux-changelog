module serie;

import tema : Tema;
import commit : Commit;

//import std.stdio : writeln;
void writeln(T...)(T args) {}

struct Serie {
    import std.typecons : Nullable, nullable;

    Tema[int] temas;
    int nivel = -1;

    void añade(Tema tema, int posición) {
//        imported!"std.stdio".writeln("EMPIZAañadiendo: ", tema, " posición ", posición);
        foreach(k, t; temas) {
//            imported!"std.stdio".writeln("añadiendo", k, " ", t);
            if (t.commit.isNull || tema.commit.isNull)
                continue;
            if (t.commit.get.hash == tema.commit.get.hash) {
//                imported!"std.stdio".writeln("commit hash", k);
                // Caso raro en el que [0/N] repite un tema de un parche posterior
                // pero con una palabra menos 
                if (k == 0) {
                    t.commit = Nullable!Commit.init;
                    this.temas[0] = t;
                    continue;
                }
            }
        }
        temas[posición] = tema;
    }

    bool tieneAlgúnCommit() {
        foreach(t; temas)
            if (t.commit != Commit.init)
                return true;
        return false;
    }
    string sacaPrimerCorreo(string site) {
        foreach(t; temas.byKeyValue)
            if (t.key == 0)
                return t.value.tema ~ "\n" ~ t.value.sacaCorreo(site);

        return "";
    }

    Nullable!Tema temaConNivel(uint nivel) {
        foreach(t; temas.byKeyValue)
            if (t.key == nivel) {
                return t.value.nullable;
            }
        return typeof(return).init;
    }

    string cadenaLarga(string sitio) {
        import std.algorithm : sort;
        import std.array;
        import std.conv : text;

        string losTemas;
        bool tieneTemasSinCommit;
        foreach(k; temas.keys.sort) {
            Tema t = temas[k];
            losTemas ~=  " Tema " ~ text(k) ~ ": " ~ text(t) ~ "\n";
            if (k && !t.commit.isNull && !t.commit.get.hash.length)
                tieneTemasSinCommit = true;
        }

        Nullable!Tema cero = temaConNivel(0);
        string asuntoCero;
        string cuerpoCero;

        if (cero.isNull) {
            // Quizás se trata de un commit único, sin nivel
            cero = temaConNivel(-1);
            if (!cero.isNull) {
                asuntoCero = cero.get.asuntoLimpio;
            }
        } else {
            asuntoCero = cero.get.asuntoLimpio ~ " ";
            writeln("asuntoCero: ", asuntoCero);
            cuerpoCero = cero.get.sacaCorreo(sitio) ~ "\n";
        }

        string[] lista;
        foreach(t; temas) {
            if (t.commit.isNull)
                continue;
            lista ~=  "[[https://git.kernel.org/linus/" ~ t.commit.get.hash ~ "|commit]]";
        }

        return cuerpoCero ~ losTemas ~ " * " ~ asuntoCero ~ lista.join(", ");
    }

    string toString(bool bonito = false) {
        import std.algorithm : sort, canFind;
        import std.array;
        import std.conv : text;
        import std.string : capitalize;

        Nullable!Tema cero = temaConNivel(0);
        string asuntoCero;

        if (!cero.isNull) {
            asuntoCero = cero.get.asuntoLimpio ~ " ";
            writeln("asuntoCero: ", asuntoCero);
        } else {
            // Quizás se trata de un commit único, sin nivel
            if (this.temas.length == 1) {
                Tema único = this.temas.byValue.front;
                asuntoCero = único.asuntoLimpio ~ " ";
            } else {
                import std.format : format;
                asuntoCero ~= "SIN TEMA: ";
                foreach(t; temas) {
                    asuntoCero ~= format("<%s> ", t.asuntoLimpio);
                }
            }
        }

        // No queremos poner mayúsculas a los prefijos tipo scsi: scsi_debug: Add new error injection type: Reset LUN failed
        if (!canFind(asuntoCero, ':')) {
            asuntoCero = asuntoCero.capitalize;
        }

        if (bonito) {
            string ret;
            foreach(i, t; temas) {
                ret ~= text(i, ": ", t.tema, ": ");
                if (t.commit.isNull) {
                    ret ~= " (null commit)";
                } else {
                    ret ~= text(t.commit.get.hash);
                }
                ret ~= "\n";
            }
            return ret;
        }

        string[] lista;
        string[] nulos;
        foreach(i, t; temas) {
            if (i == 0 && temas.length > 1) {
                // FUTURO: Aquí enlace al [0/N] de la lista
                continue;
            }
            if (t.commit.isNull) {
                nulos ~= t.tema;
                continue;
            }
            lista ~=  "[[https://git.kernel.org/linus/" ~ t.commit.get.hash ~ "|commit]]";
        }

        string nuloStr;
        if (nulos.length) {
            import std.algorithm : map;
            nuloStr = "NULOS: ";
            nuloStr ~= nulos.map!(tema => "<" ~ tema ~ ">").join(" ");
        }

        return " * " ~ nuloStr ~ asuntoCero ~ lista.join(", ");
    }

    void sacaCommitsDeTemas(string gitRepo, string[] log) {
        foreach(i, t; this.temas) {
            if (i == 0) {
                t.sacaCommit(gitRepo, log, false);
            } else {
                t.sacaCommit(gitRepo, log);
            }

            this.añade(t, i);
        }
    }

    bool opEquals(ref Serie otra, bool detallada = false) {
	writeln("serie comparación detallada ", detallada, " de ", this, " con ", otra);
        if (this.nivel != otra.nivel)
            return false;
        if (this.temas.length != otra.temas.length)
            return false;
        foreach(t; this.temas.byKeyValue) {
            import std.algorithm : canFind;
		writeln("tema comparado ", t.value);
            if (!canFind(otra.temas.keys, t.key)) {
		writeln("serie DESIGUAL, sin clave ", t.key);
                return false;
            }
            if (!t.value.opEquals(otra.temas[t.key], detallada)) {
		writeln("serie DESIGUAL ", t.key, "\notro ", otra.temas[t.key]);
                return false;
            }
        }
		writeln("serie IGUAL ");
        return true;
    }

    bool comparaPrint(ref Serie otra) {
        import std.conv : text;
        void print(string str) {
            imported!"std.stdio".writeln(str);
        }

        bool correcto = true;

        if (this.nivel != otra.nivel) {
            print("Diferente nivel: this " ~ text(this.nivel) ~ " vs " ~ text(otra.nivel));
            correcto = false;
        }
        if (this.temas.length != otra.temas.length) {
            print("Diferente longitud de temas: this " ~ text(this.temas.length) ~ " vs " ~ text(otra.temas.length));
            correcto = false;
        }

        foreach(t; this.temas.byKeyValue) {
            import std.algorithm : canFind;
            if (!canFind(otra.temas.keys, t.key)) {
                correcto = false;
                break;
            }
            if (!t.value.opEquals(otra.temas[t.key], true)) {
                correcto = false;
                break;
            }
        }
        if (!correcto) {
            import std.conv : text, to;
            import std.algorithm : max;
            auto maximo = max(this.temas.length, otra.temas.length).to!int;

            foreach(i; 0 .. maximo) {
                string buf;
                buf ~= text(i, ": ");
                if (auto tema2 = i in this.temas) {
                    if (auto tema1 = i in otra.temas) {
                        if (*tema1 == *tema2) {
                            buf ~= "IGUALES (" ~ tema1.tema;
                            if (tema1.commit.isNull) {
                                buf ~= "null commit";
                            } else {
                                buf ~= tema1.commit.get.hash;
                            }
                            buf ~= ")";
                            print(buf);
                            continue;
                        }
                    }
                }
                buf ~= "NO SON IGUALES\n";
                if (auto tema = i in this.temas) {
                    buf ~= tema.tema;
                    if (tema.commit.isNull) {
                        buf ~= " (null commit)";
                    } else {
                        buf ~= " " ~ tema.commit.get.hash;
                    }
                } else {
                    buf ~= "TEMA NO EXISTE";
                }
                buf ~= "\n";

                if (auto tema = i in otra.temas) {
                    buf ~= tema.tema;
                    Nullable!Commit c = tema.commit;
                    if (tema.commit.isNull) {
                        buf ~= "(null commit)";
                    } else {
                        buf ~= " " ~ tema.commit.get.hash;
                    }
                } else {
                    buf ~= "TEMA NO EXISTE";
                }
                print(buf);
            }
        }
        return correcto;
    }

    import diferencia : juntaDiferencias, Diferencia;
    Diferencia[] diferencias() {
        return juntaDiferencias(this);
    }
}


