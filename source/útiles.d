module útiles;

import commit : Commit, dameUnCommit;
import serie : Serie;
import lei : encuentraTemaLei, encuentraTemaLeiEnJson, consigueJson;
import mantenedores : ÁreaMantenida;
import conf : sitio;
import std.json : JSONValue;

//import std.stdio : writeln;
void writeln(T...)(T args) {}

struct GitLogCache {
    string[] entradasLog;
    string últimoCommit;
}

import std.typecons : Nullable, nullable;

Nullable!GitLogCache cargaCache(string archivoCaché) {
    import std.file : exists, readText;
    import std.array : array;
    import std.algorithm : map;
    import std.json : parseJSON;

    if (exists(archivoCaché)) {
        GitLogCache cache;

        auto json = readText(archivoCaché).parseJSON;
        cache.entradasLog = json["entradasLog"].array.map!(x => x.str).array;
        cache.últimoCommit = json["ultimoCommit"].str;
        return cache.nullable;
    }

    return typeof(return).init;
}

string sanea(string cad) {
    import std.conv : text;
    import std.algorithm : map, canFind;

    static invalidos = "/\\:*?\"<>| ";
    string sano = cad.map!(c => canFind(invalidos, c) ? '-' : c).text;
    return "LEI_" ~ sano;
}

JSONValue cargaLeiCache(string asunto, string path) {
    import std.file : readText;
    import std.json : parseJSON;
    import std.path : buildPath;

    auto cachePath = buildPath(path, asunto.sanea);
    return readText(cachePath).parseJSON;
}

void almacenaLeiCache(string asunto, string path) {
    import std.file : write;
    import std.path : buildPath;
    import std.json : toJSON;

    auto cachePath = buildPath(path, asunto.sanea);
    JSONValue json = consigueJson(sitio, asunto);
    write(cachePath, json.toJSON(true));
}

void descargaCache(GitLogCache caché, string archivo) {
    import std.file : write, mkdirRecurse;
    import std.path : dirName;

    auto json = JSONValue(["entradasLog": JSONValue(caché.entradasLog),
                           "ultimoCommit": JSONValue(caché.últimoCommit)]);

    mkdirRecurse(dirName(archivo));
    write(archivo, json.toPrettyString);
}

string últimoCommit(string gitRepo) {
    import std.process : execute;
    import std.exception : enforce;
    import std.string : strip;

    auto proc = execute(["git", "show", "master", "--pretty=format:%H", "--no-patch"], workDir: gitRepo);
    enforce(proc.status == 0, proc.output);
    return proc.output.strip;
}

string[] dameLog(string gitRepo) {
    import std.process : Config, executeShell, environment;
    import std.exception : enforce;
    import std.string : splitLines;
    import std.uni : toLower;
    import std.path : buildPath;

    string commit = últimoCommit(gitRepo);

    string archivo = buildPath(environment.get("HOME", "/tmp"), ".cache", "linux-changelog", "cache_linux.json");
    Nullable!GitLogCache caché = cargaCache(archivo);

    if (!caché.isNull && (caché.get.últimoCommit == commit)) {
        return caché.get.entradasLog;
    }

    auto proc = executeShell("git log --pretty=oneline", null, Config.none, ulong.max, gitRepo);
    enforce(proc.status == 0, proc.output);
    string[] commits = proc.output.toLower.splitLines;

    auto nuevoCaché = GitLogCache(commits, commit);
    descargaCache(nuevoCaché, archivo);

    return nuevoCaché.entradasLog;
}

string sacaTemaDeListaCambios(string desdeLista) {
    import std.regex : matchFirst, regex, Captures, Regex;
    import std.string : strip;

    enum Regex!char expr = regex(`(.*\* )?(?P<tema>[^\[]*)?(\[\[.*)?`);
    Captures!string match = matchFirst(desdeLista, expr);

    return match["tema"].strip;
}

Serie[] seriesDeCommits(Commit[] commits, string[] log, string gitRepo, string sitio) {
    bool yaEstáEnSeries(Commit candi, Serie[] lista) {
        foreach(s; lista) {
            foreach(t; s.temas) {
                if (t.commit.isNull)
                    continue;
                if (t.commit.get.hash == candi.hash) {
                    return true;
                }
            }
        }
        return false;
    }

    Serie[] ret;

    foreach(c; commits) {
        if (yaEstáEnSeries(c, ret))
            continue;

        Serie serie = encuentraTemaLei(c.asunto, log, gitRepo, sitio);
        ret ~= serie;
    }

    return ret;
}

Serie[] seriesDeAsuntos(string[] asuntos, string[] log, string gitRepo, string sitio, string cachePath, bool cache = false) {
    bool yaEstáEnSeries(string asunto, Serie[] lista) {
        foreach(s; lista) {
            foreach(t; s.temas) {
                import std.algorithm : canFind;
                import std.uni : toLower;
                if (canFind(t.asuntoLimpio.toLower, asunto.toLower)) {
                    return true;
                }
            }
        }
        return false;
    }

    Serie[] ret;

    foreach(a; asuntos) {
        if (yaEstáEnSeries(a, ret))
            continue;

        if (cache) {
            almacenaLeiCache(a, cachePath);
        }
        auto leiCache = cargaLeiCache(a, cachePath);
        Serie serie = encuentraTemaLeiEnJson(a, log, gitRepo, sitio, leiCache);

        ret ~= serie;
    }

    return ret;
}

Commit[] commitsDeShas(string gitRepo, string[] shas) {
    Commit[] ret;
    foreach(s; shas)
        ret ~= dameUnCommit(gitRepo, s);
    return ret;
}

string[] commitsDeCorreos(string path) {
    import std.array : array;
    import std.file : DirEntry, dirEntries, SpanMode, isFile;
    import std.algorithm : filter;
    import std.regex : matchFirst, Captures, regex, Regex;

    static Regex!char gitHeader = regex(`X-Git-Rev: (?P<id>[A-Fa-f0-9]{40})`);
    string[] ret;

    DirEntry[] files = dirEntries(path, SpanMode.depth)
        .filter!(a => a.isFile)
        .array;

    foreach(file; files) {
        import std.file : readText;

        string buf = readText(file);
        if (buf == "") {
            imported!"std.stdio".writeln("WARNING: Empty mail " ~ file);
            continue;
        }
        Captures!string match = matchFirst(buf, gitHeader);
        if (!match.empty)
            ret ~= match["id"];
    }

    return ret;
}

string[] asuntosDeCorreos(string path) {
    import std.array : array;
    import std.file : DirEntry, dirEntries, SpanMode, isFile;
    import std.algorithm : filter;
    import std.regex : matchFirst, Captures, regex, Regex;

    static Regex!char gitHeader = regex(`Subject: (?P<subject>.*)`);
    string[] ret;

    DirEntry[] files = dirEntries(path, SpanMode.depth)
        .filter!(a => a.isFile)
        .array;

    foreach(file; files) {
        import std.file : readText;

        string buf = readText(file);
        if (buf == "") {
            imported!"std.stdio".writeln("WARNING: Empty mail " ~ file);
            continue;
        }
        Captures!string match = matchFirst(buf, gitHeader);
        if (!match.empty)
            ret ~= match["subject"];
    }

    return ret;
}

