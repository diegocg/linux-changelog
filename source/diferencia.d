module diferencia;

//import std.stdio : writeln;
void writeln(T...)(T args) {}

struct Diferencia {
    ulong añadidas;
    ulong borradas;
    string ruta;

    string toString() {
        import std.conv : text;
        return ruta ~ ": " ~ "+" ~ text(añadidas) ~ "-" ~ text(borradas);
    }
}

// Se prioriza la diferencia con mayor cantidad de cambios
// Cada diferencia es el nombre de archivo y el número de + y -
// Ordenadas por número de cambios
Diferencia[] ordenaDiferencias(string[] diffs) {
    import std.algorithm : any, filter, map, sort;
    import std.range : array;
    import std.conv : to;

    // string[][]
    auto pre = diffs.map!((diff) {
        import std.regex : matchFirst, regex, Regex, Captures;

        Regex!char expr = regex(`(?P<add>\S+)\t(?P<del>\S+)\t(?P<file>.*)`);
        Captures!string match = matchFirst(diff, expr);
        assert(match.empty == 0, "Error parsing diffstat: " ~ diff);
        return [match["add"], match["del"], match["file"]];
    });

    Diferencia[] regs = pre
        .filter!(stat => stat[0] != "-" && stat[1] != "-")
        .array
        .sort!((a, b) => a[0].to!long > b[0].to!long)
        .map!(d => Diferencia(d[0].to!long, d[1].to!long, d[2]))
        .array;

    return regs;
}

Diferencia[] juntaDiferencias(imported!"serie".Serie serie) {
    import std.algorithm : joiner, map, sort, sum, filter, chunkBy;
    import std.array : array;

    auto difs = serie.temas
        .values
        .filter!(t => !t.commit.isNull)
        .map!(t => t.commit.get.diferencias)
        .joiner
        .array
        .sort!((a, b) => a.ruta < b.ruta)
        .chunkBy!((a, b) => a.ruta == b.ruta)
        .map!((g) {
            long añadidas = g.map!(el => el.añadidas).sum;
            long borradas = g.map!(el => el.borradas).sum;
            return Diferencia(añadidas, borradas, g.array[0].ruta);
        })
        .array;

    // Las más importantes son las que tienen un número mayor de añadidos totales
    long resta(Diferencia a) {
        return cast(long) a.añadidas - a.borradas;
    }
    auto ordena = difs
        .sort!((a, b) => resta(a) > resta(b))
        .array;

    return ordena;
}

