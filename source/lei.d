module lei;

import serie : Serie;
import tema : Tema;
import commit : Commit;
import std.json : JSONValue;

//import std.stdio : writeln;
void writeln(T...)(T args) {}

private string quitarHasta(string en, string car) {
    import std.range : retro;
    import std.string : indexOf;
    import std.conv : to;

    auto revés = en.retro.to!string;
    auto ind = revés.indexOf(car);
    return ind != -1 ? revés[0..ind].retro.to!string : en;
    
}


private bool coincidenciaSinPrefijo(string str1, string str2) {
    import std.algorithm : canFind;

    string str11 = str1.quitarHasta(":");
    string str21 = str2.quitarHasta(":");
    return canFind(str11, str21) || canFind(str21, str11);
}

import std.regex : regex, Regex;

// [PATCH 3\/4] ARM: dts: stm32: add pin map for CAN controller on stm32f
const Regex!char exprMultiple = regex(`[vV]?(?P<version>\d+)?\s*?(?P<serie>\d+)[\\/]+(?P<max>\d+)\]\s*(?P<asunto>\w.*)`);

// [PATCH v1] thermal: intel: menlow: Get rid of this driver"
const Regex!char exprSingle = regex(` [vV](?P<version>\d+)\]`);

// [PATCH] serial: Add support for Advantech PCI-1611U card
const Regex!char patch = regex(`\[PATCH\] (?P<tema>.*)`);

// Detectar asuntos de -stable, con 3 dígitos
// [PATCH 5.15 060\/203] gfs2: Fix inode height consistency check
auto esStable(string asunto) {
    import std.regex : matchFirst;
    const Regex!char stable = regex(`\[PATCH \d.\d\d? \d\d\d\\/\d\d\d\]`);
    return matchFirst(asunto, stable);
}

JSONValue consigueJson(string sitio, string asunto) {
    import std.json : parseJSON;
    import std.process : execute, Config;
    import std.conv : text;

    // lei q -I https://lore.kernel.org/all/ s:"Add folio dtor and order setter functions" --threads
    // /usr/bin/curl -Sf -s -d '' https://lore.kernel.org/all/?x=m&t=1&q=s%3A%22Add+folio+dtor+and+order+setter+functions%22
    string[] cmd = ["lei", "q", "--no-save", "-I", sitio, "-q", "-o", "-", "--threads", "s:" ~ asunto];

    auto proc = execute(cmd, null, Config.none, ulong.max);
    assert(proc.status == 0, "lei: " ~ proc.text);


    JSONValue json = parseJSON(proc.output);
    return json;
}

Serie encuentraTemaLei(string asunto, string[] log, string gitRepo, string sitio) {
    auto jsons = consigueJson(sitio, asunto);
    return encuentraTemaLeiEnJson(asunto, log, gitRepo, sitio, jsons);
}

struct PrimerFiltro {
    string asunto;
    string msgID;
    string[] referencias;
}

PrimerFiltro[] primerFiltro(JSONValue json, string asunto) {
    import std.conv : to;
    import std.regex : matchFirst, Captures;
    import std.algorithm : startsWith, canFind, map;
    import std.array : array;
    import std.uni : toLower;

    PrimerFiltro[] asuntos;
    foreach(e; json.array) {
        if (e.isNull)
            continue;

        string asun = e["s"].str;

        // El GIT PULL es para evitar un problema con "crypto: deflate - Remove zlib-deflate"
        // Resulta que lo ubicamos en 0 sin motivo y el veradero se ignora
        if (startsWith(asun.toLower, "re:") ||
            canFind(asun, "[PATCH AUTOSEL") ||
            startsWith(asun, "[GIT PULL")) {
            continue;
        }

        if (asun.esStable) {
            continue;
        }

        // Caso especial: "crypto: deflate - Remove zlib-deflate"
        // Tiene una serie de seis parches y luego uno previo de RFC
        // Ignorar el RFC si ya se ha encontrado algo
        // No es buena idea: No encontraria los RFCs aislados 
        if (canFind(asun, "[RFC")) {
            continue;
        }

        Captures!string metaMultiple = matchFirst(asun, exprMultiple);


        // Si es uno de -stable que tiene cientos de mensajes en el hilo
        if (!metaMultiple.empty && (metaMultiple["max"].to!int > 60)) {
            continue;
        }

        string[] refs;
        if (auto rs = "refs" in e) {
            refs = rs.array.map!(el => el.str).array;
        }
        asuntos ~= PrimerFiltro(asun, e["m"].str, refs);
    }

    return asuntos;
}

bool esHijo(PrimerFiltro tema, Serie serie) {
    foreach(referencia; tema.referencias) {
        foreach(t; serie.temas) {
            if ("<" ~ referencia ~ ">" == t.messageID) {
                return true;
            }
        }
    }

    return false;
}

Serie encuentraTemaLeiEnJson(string asunto, string[] log, string gitRepo, string sitio, JSONValue json) {
    import std.regex : matchFirst, Captures;
    import std.string : strip;
    import std.conv : to;
    import std.algorithm : startsWith, canFind;
    import std.uni : toLower;

    Serie serie;

    void añade(Tema t, int nivel) {
        if (serie.temas.values.canFind!(tem => tem == t)) {
            return;
        }
        serie.añade(t, nivel);
    }

    writeln("encuentraTemaLei, asunto requerido ", asunto);
    PrimerFiltro[] asuntos = primerFiltro(json, asunto);
    writeln("Primer filtro:");
    foreach(a; asuntos) {
        writeln(a);
    }

    foreach(primer; asuntos) {
        string asun = primer.asunto;
        string msgID = primer.msgID;
        string[] referencias = primer.referencias;

        Captures!string metaMultiple = matchFirst(asun, exprMultiple);
        Captures!string metaSingle = matchFirst(asun, exprSingle);
writeln("METAMULTIPLE", metaMultiple, "\nMETASINGLE", metaSingle);

        // Si coincide el tema y no hemos añadido, al menos con nivel que no sea -1
        if (coincidenciaSinPrefijo(asunto, asun) && serie.nivel == -1) {
            if (metaMultiple.empty && metaSingle.empty) {
                auto parche = matchFirst(asun, patch);
                // Si tenemos un PATCH sin versión, y no hemos tocado actual, se trata de un
                // asunto que podemos añadir a la serie, aunque sea como tema único
                if (parche) {
                    import std.exception : enforce;

                    enforce(parche["tema"] != "");
                    writeln("añadiendo tema 1 ", parche);
                    Tema t = Tema(parche["tema"].strip).sacaCommit(gitRepo, log).añadeMessageID(msgID).añadeReferencias(referencias);
                    writeln("añadiendo ", t);
                    añade(t, -1);
                }
                writeln("continuando 1");
                continue;
            }

            if (!metaSingle.empty) {
                writeln("añadiendo tema 2 con nivel ", metaSingle["version"]);
                Tema t = Tema(asun).sacaCommit(gitRepo, log, false).añadeMessageID(msgID).añadeReferencias(referencias);
                writeln("añadiendo ", t);
                añade(t, 0);
                serie.nivel = metaSingle["version"].to!int;
                writeln("continuando 2");
                continue;
            }

        }

        auto ver = !metaMultiple.empty ? metaMultiple["version"] : metaSingle["version"];
        int numSerie = !metaMultiple.empty ? metaMultiple["serie"].to!int : 0;

        int nivel;

        if (ver !is null) {
            nivel = ver.to!int;

            if (nivel < serie.nivel)
                continue;
            if (nivel > serie.nivel) {
                writeln("LIMPIANDO temas, porque ", nivel, " > ", serie.nivel, ", serie: ", serie.temas);
                serie.nivel = nivel;
                serie.temas.clear;
            }
        // numSerie != 0 porque hay algunas series con hijos que no tienen número
        // ej: "locking: Introduce local{,64}_try_cmpxchg"
        } else if (esHijo(primer, serie) && numSerie != 0) {
            writeln("preparando tema HIJO");
            Tema t = Tema(asun).sacaCommit(gitRepo, log).añadeMessageID(msgID).añadeReferencias(referencias);
            writeln("añadiendo ", t);
            añade(t, numSerie);
            continue;
        } else if (serie.nivel != -1) {
            // Si tenemos una serie [PATCH v4 1/4], y de repente encontramos un [PATCH 1/4]
            writeln("continuando 3");
            continue;
        }

        // Aquí nivel == actual, añadimos temas de ese nivel a la serie
        Tema t = Tema(asun).sacaCommit(gitRepo, log).añadeMessageID(msgID).añadeReferencias(referencias);
        writeln("añadiendo tema 3: ", t, " con nivel ", numSerie);
        añade(t, numSerie);
    }

    // No hemos encontrado nada, intentemos al menos sacar el commit
    if (!serie.temas) {

        Tema t = Tema(asunto).sacaCommit(gitRepo, log);
        writeln("No hay temas, añadiendo tema con asunto -1 ", t);
        añade(t, -1);
    }

//    serie.ordena();

    foreach(i, t; serie.temas) {
        writeln(i, " ", t);
    }
    return serie;
}
