module tema;

import std.typecons : Nullable;

import commit : Commit, dameUnCommit;
import diferencia : ordenaDiferencias;

//import std.stdio : writeln;
void writeln(T...)(T args) {}

struct Tema {
    string tema;
    string asuntoLimpio;
    Nullable!Commit commit;
    string messageID;
    string[] referencias;

    this(string t) {
        string sinCorchetes = quitaCorchetes(t);
	writeln("tema ctor ", sinCorchetes);
        this.tema = sinCorchetes;
        this.asuntoLimpio = limpiaAsunto(sinCorchetes);
    }

    this(string t, string c, bool detallesCommit = false, string gitRepo = "") {
	writeln("tema ctor 2 ", t, " commit ", c);
	auto sinCorchetes = quitaCorchetes(t);
        this.tema = sinCorchetes;
        this.asuntoLimpio = limpiaAsunto(sinCorchetes);
        this.commit = Commit(c, detallesCommit, gitRepo);
//        assert(0);
    }

    Tema sacaCommit(string gitRepo, string[] log, bool cortaAsunto = true) {
        import std.string : strip, toLower;

//        imported!"std.exception".enforce(this.commit.isNull);
        string asunto = this.asuntoLimpio;

        // Si puedo sacarlo completo, entonces sácalo, al margen de cortaAsunto
        Nullable!Commit c = sacaCommitDeTema(this.tema.toLower.strip, gitRepo, log);
        if (!c.isNull) {
            this.commit = c;
            return this;
        }

        writeln("sacaCommit, cortar?", cortaAsunto, ": ", asunto);
        if (cortaAsunto) {
            asunto = asunto.sinDosPuntos.toLower;
        }

        c = sacaCommitDeTema(asunto, gitRepo, log);
	writeln("sacaCommit ", tema, " commit ", c);
        this.commit = c;
        return this;
    }

    Tema añadeMessageID(string msgID) {
        this.messageID = (msgID[0] == '<' && msgID[$-1] == '>') ? msgID : "<" ~ msgID ~ ">";
        return this;
    }

    Tema añadeReferencias(string[] referencias) {
        this.referencias = referencias;
        return this;
    }

    string sacaCorreo(string site) {
        import std.process : execute, Config;
        import std.conv : to;
        import std.string : strip, splitLines;
        import std.algorithm : startsWith, joiner;

        // lei lcat -f text -I https://lore.kernel.org/all/ "<20221031231556.a15846fd3513641d48820d5b@kernel.org>
        auto proc = execute(["lei", "lcat", "-I", site, messageID], null, Config.none, ulong.max);
        assert(proc.status == 0, "lei: " ~ proc.to!string);
        string quitaFinal(string cuerpo) {
            string[] ret;
            foreach(l; cuerpo.splitLines) {
                if (l.startsWith("---"))
                    break;
                ret ~= l;
            }

            return ret.joiner("\n").to!string;
        }
        string quitaCabeceras(string cuerpo) {
            string[] ret;
            bool añade;
            foreach(l; cuerpo.splitLines) {
                if (añade)
                    ret ~= l;
                if (l.startsWith("Message-ID: "))
                    añade = true;
            }
            return ret.joiner("\n").to!string;
        }
        string quitaTodo(string cuerpo) {
            return quitaFinal(quitaCabeceras(cuerpo));
        }

        return quitaTodo(proc.output).strip;
    }

    string toString() {
        string c = commit.isNull ? "" : commit.get.hash;
        string cadena = asuntoLimpio ~ " <" ~ c ~ ">";
        return cadena;
    }

    bool opEquals(Tema otro, bool detailed = false) {
	writeln("Tema.opEquals CMP ", this, "\notro ", otro);
        if (this.tema != otro.tema) {
	writeln("TEMA DESIGUAL: <", this.tema, ">\notro: <", otro.tema, ">");
            return false;
        }
        writeln("TEMA FUE IGUAL");
        if (this.asuntoLimpio != otro.asuntoLimpio) {
	writeln("TEMA DESIGUAL:2 ", this, "\notro: ", otro);
            return false;
        }
        writeln("ASUNTO LIMPIO FUE IGUAL");
	writeln("TEMA commit is null ", this.commit.isNull, " ", otro.commit.isNull);
        if ((this.commit.isNull == otro.commit.isNull) && (!this.commit.isNull) &&
            (!this.commit.get.opEquals(otro.commit.get, detailed))
        ) {
            writeln("TEMA DESIGUAL:3 ", this, "\notro: ", otro);
            return false;
        }
	writeln("TEMA CMP, es IGUAL");
        return true;
    }
}

Nullable!Commit sacaCommitDeTema(string asunto, string gitRepo, string[] log) {
    writeln("sacaCommitDeTema, asunto ", asunto, " log ", log.length);
    import std.algorithm : canFind;
    import std.exception : enforce;
    import std.uni : toLower;

//        if (this.serie == 0)
//            return Commit.init;
    enforce(asunto != "");

    Nullable!Commit ret;
    foreach(l; log) {
        if (canFind(l, asunto)) {
            ret = dameUnCommit(gitRepo, l[0 .. 40]);
//            return ret;
        }
    }

    return ret;
}

private string eliminaDosPuntosEspacios(string entrada) {
    import std.string : indexOf;

    do {
        auto idx = indexOf(entrada, " : ");
        if (idx == -1) {
            return entrada;
        }
        entrada = entrada[0 .. idx] ~ entrada[idx+1 .. $];
    } while (true);
}

private string sinDosPuntos(string entrada) {
    import std.range : retro;
    import std.string : indexOf, strip;
    import std.conv : to;
    import std.algorithm : count;

    auto inverso = entrada.retro.to!string;
    auto idx1 = inverso.indexOf(":");

    if (idx1 == -1) {
        return entrada;
    }

    entrada = entrada.strip;
    entrada = entrada[$-1] == '.' ? entrada[0 .. $-1] : entrada;
    entrada = entrada.eliminaDosPuntosEspacios;

    auto pos = entrada.length - idx1;

    // Si tiene más de un espacio en el prefijo, igual no merece la pena romperlo
    auto espacios = entrada[0 .. pos].count!("a == ' '");
    if (espacios > 1) {
        return entrada;
    }

    string sinEspacios = entrada[pos .. $].strip;

    return sinEspacios;
}

string limpiaAsunto(string tema) {
    import lei : exprMultiple;
    import std.regex : matchFirst, Captures, regex, Regex;
    import std.conv : text;

    enum prefijo = regex(`^(?P<prefijo>\w{0,15}:)\s+(?P<resto>.+)`);
    string reemp(string s) {
        import std.string : strip;
        import std.array : replace;
        return s.replace(`\/`, `/`).replace(`\\`, `\`).replace(`//`, `/`).strip;
    }

    string límpio = tema;

    Captures!string meta = matchFirst(tema, exprMultiple);
//    writeln("\nlimpiaAsunto for tema ", tema, "\nmatched meta: ", meta);

    if (!meta.empty) {
//        writeln("poniendo limpio a ", meta["asunto"]);
        límpio = meta["asunto"];
    }

    auto quita = matchFirst(límpio, prefijo);
// /  writeln("quita ", quita);

    if (!quita.empty) {
//        writeln("poniendo 2 limpio a ", quita["resto"]);
        límpio = quita["resto"];
    }

    return reemp(límpio);
}

string quitaCorchetes(string tema) {
    import std.string : strip;
    import std.regex : matchFirst, regex;

    auto corchete = regex(`\[.*\](?P<tema>.*)`);
    auto m = matchFirst(tema.strip, corchete);
    writeln("para ", tema, " salió ", m);

    if (!m) {
        return tema.strip;
    }

    return m["tema"].strip;
}
