module lista;

import diferencia : Diferencia;
import commit : Commit;
import mantenedores : ÁreaMantenida;
import serie : Serie;

//import std.stdio : writeln;
void writeln(T...)(T args) {}

// estricto hace que se extraigan las áreas mantenidas que coinciden exactamente con la ruta,
// es decir, rutas que tienen un gran mantenedor tras ellas
private ÁreaMantenida[] filtraÁreas(ÁreaMantenida[] lista, string[] rutas, bool estricto = false) {
    import std.algorithm : filter, any, startsWith;
    import std.array : array;

    ÁreaMantenida[] ret;
    foreach(área; lista) {
        ÁreaMantenida tmp;
        tmp.nombre = área.nombre;
        if (estricto) {
            tmp.entradas = área.entradas
                .filter!(ent => rutas.any!((ruta) {
                    string conBarra = ruta[$ - 1] == '/' ? ruta : ruta ~ "/";
                    return ent.expre == conBarra;
                }))
                .array;
        } else {               
            tmp.entradas = área.entradas
                .filter!(ent => rutas.any!(r => ent.expre.startsWith(r)))
                .array;
        }
        ret ~= tmp;
    }
    return ret.filter!(área => área.entradas.length > 0).array;
}

private Commit[] filtraCommits(Commit[] lista, ulong índiceDif, ÁreaMantenida área) {
    import std.regex : Captures, regex, matchFirst, escaper, Regex;
    import std.conv : text;

    Commit[] ret;
    foreach(c; lista) {
        if (c.diferencias.length <= índiceDif)
            continue;
        Diferencia buscarEn = c.diferencias[índiceDif];
        foreach(entrada; área.entradas) {
            Regex!char expr = regex(entrada.expre.escaper.text);
            Captures!string match = matchFirst(buscarEn.ruta, expr);
            if (!match.empty)
                ret ~= c;
        }
    }
    return ret;
}

private Serie[] filtraSeries(Serie[] series, ulong índiceDif, ÁreaMantenida área) {
    import std.regex : Captures, regex, matchFirst, escaper, Regex;
    import std.conv : text;

    Serie[] ret;
    foreach(serie; series) {
        Diferencia[] difGrupo = serie.diferencias;
        if (difGrupo.length <= índiceDif)
            continue;
        Diferencia buscarEn = difGrupo[índiceDif];
        foreach(entrada; área.entradas) {
            Regex!char expr = regex(entrada.expre.escaper.text);
            Captures!string match = matchFirst(buscarEn.ruta, expr);
            if (!match.empty)
                ret ~= serie;
        }
    }
    return ret;
}

ÁreaMantenida[] filtraÁrea(ÁreaMantenida[] áreas, Serie serie) {
    import mantenedores : Entrada, ÁreaMantenida;
    import std.algorithm : all, sort, canFind;
    import std.regex : regex, matchFirst, escaper, Regex;
    import std.array : array;
    import std.conv : text;

    bool difCasaConEntrada(Diferencia dif, Entrada ent) {
        Regex!char expr = regex("^" ~ ent.expre.escaper.text);
        auto coincide = !matchFirst(dif.ruta, expr).empty;
        return coincide;
    }

    ÁreaMantenida[] comparaDifConÁrea(Diferencia dif, ÁreaMantenida área) {
        ÁreaMantenida[] ret;
        foreach(entrada; área.entradas) {
            if (difCasaConEntrada(dif, entrada)) {
writeln("añadiendo ", área, " coincidió ", entrada.expre);
                auto nuevaÁrea = área;
                // Un área puede tener muchas entradas, añade sólo la relevante, aun
                // si luego hay repeticiones de áreas
                nuevaÁrea.entradas = [entrada];
                ret ~= nuevaÁrea;
            }
        }
        return ret;
    }

    // Las diferencias deberían estar ordenadas por "importancia", definida como
    // aquella con la (.añadidas - .borradas) más grande.
    //
    // Este ordenamiento es importante a la hora de encontrar entradas (creo)
    // 
    // Pero una vez encontradas las entradas, reordenamos los resultados por las entradas
    // con la expresión más larga - aquella con más probabilidades de ser más significativa
    // de a qué sección pertenece la serie
    ÁreaMantenida[] todas;
writeln("filtraÁrea: START");
    foreach(dif; serie.diferencias) {
writeln("filtraÁrea: dif ", dif);
        ÁreaMantenida[] buf;
        foreach(área; áreas) {
            ÁreaMantenida[] nuevasDiferencias = comparaDifConÁrea(dif, área);
            // desduplica
            foreach(n; nuevasDiferencias) {
                if (!(canFind(buf, n) || canFind(todas, n))) {
                    buf ~= n;
                }
            }
        }
        todas ~= buf;
    }
    return todas
        // Cada .entradas debería tener sólo un elemento
        .sort!((a, b) => a.entradas[0].expre.length > b.entradas[0].expre.length)
        .array;
}

string generaListaConRelaciones(Serie[] series, ÁreaMantenida[] áreas, string gitRepo, string sitio) {
    import std.algorithm : map, sort, each, maxElement;
    import std.range : array, repeat, join;
    import std.array : Appender, appender, byPair;

    auto asociaciones = [
        "Core": ["kernel"],
        "File systems": ["fs"],
        "Memory management": ["mm"],
        "Networking": ["net", "drivers/net", "include/net"],
        "Graphics": ["drivers/gpu"],
        "Security": ["security"],
        "Virtualization": ["virt"],
        "Architectures": ["arch"],
    ];

    Appender!string lista = appender!string;

    // Iterar lista de asociaciones de arriba
    // Por cada asociación, sacar los Subsys coincidentes (que convertiremos en menús de segundo nivel)
    // Por cada Subsys, se imprimen los emails relacionados
    foreach(subsys, rutas; asociaciones.byPair) {
        lista.put("= " ~ subsys ~ " =\n");
        ÁreaMantenida[] filtradas = filtraÁreas(áreas, rutas);
        auto foo = filtraÁreas(áreas, rutas, true);
        foreach(área; filtradas) {
            Serie[] filtrados = filtraSeries(series, 0, área);
            if (filtrados.length > 0) {
                lista.put("== " ~ área.nombre ~ " ==\n");
                foreach(serie; filtrados)
                    lista.put(serie.toString  ~ "\n");
                lista.put("\n");
                foreach(sg; filtrados) {
                    import std.algorithm : countUntil, remove;
                    long idx = series.countUntil!(c => c == sg);
                    if (idx == -1) imported!"std.stdio".writeln("ERROR1, NOT FOUND");
                    series = series.remove(idx);
                }
            }
        }
    }

    // Hemos imprimido las asociaciones
    // Ahora, el remanente de correos, pero clasificado por subsys
    // sacarCorreos busca, para las expresiones regulares de cada subsys, la coincidencia en
    // la primera diferencia de cada correo, que no tiene por qué encontrar nada
    // Luego, de los correos restantes, buscamos en cada subsys (es decir, el proceso contrario)
    // De los que quedan, los imprimimos directamente
    //
    // 1. Iteración de subsystem
    //  2. Para cada subsys, buscar todos los correos que tengan en su primera diferencia
    //   3. Una vez iterados TODOS los subsistemas, buscar en la segunda diferencia. Para esto
    //      Necesitamos saber, en primera lugar, el número máximo de diferencias.
    // Debido a los anteriores requisitos, es imprescindible
    //  1. Saber de antemano el número máximo de diferencias
    //  2. Acumular los resultados para cada subsistema hasta el final

    ulong[] longitudes = series
        .map!((s) => s.diferencias.length)
        .array;
    ulong máxDif;
    if (longitudes.length > 0)
        máxDif = longitudes.maxElement;

    lista.put("== SIN ASOCIACIONES ==\n");
    foreach(numDiferencia; 0 .. máxDif) {
        foreach(área; áreas) {
            Serie[] filtrados = filtraSeries(series, numDiferencia, área);
            if (filtrados.length == 0)
                continue;

            string temp;
            foreach(serie; filtrados)
                temp ~= serie.toString ~ "\n";
            lista.put("= " ~ área.nombre ~ " =\n");
            lista.put(temp ~ "\n");

            foreach(sg; filtrados) {
                import std.algorithm : countUntil, remove;
                long idx = series.countUntil!(c => c == sg);
                if (idx == -1) imported!"std.stdio".writeln("ERROR, NOT FOUND");
                series = series.remove(idx);
            }
        }
    }

    lista.put("== SOBRAS ==\n");
    foreach(serie; series)
        lista.put(serie.toString ~ "\n");
    lista.put("\nFIN");

    return lista[];
}

