module commit;

import std.datetime.date : DateTime;
import diferencia : Diferencia, ordenaDiferencias;

struct Commit {
    string hash;
    DateTime fechaCommit;
    string asunto;
    string árbol;
    string[] padre;
    string correo;
    string registrador;
    DateTime fechaRegistro;
    Diferencia[] diferencias;

    this(string hash, DateTime fechaCommit, string asunto, string árbol, string[] padre, string correo, string registrador, DateTime fechaRegistro, Diferencia[] diferencias) {
        this.hash = hash;
        this.fechaCommit = fechaCommit;
        this.asunto = asunto;
        this.árbol = árbol;
        this.padre = padre;
        this.correo = correo;
        this.registrador = registrador;
        this.fechaRegistro = fechaRegistro;
        this.diferencias = diferencias;
    }

    string toString() {
        import std.format : format;
        return format!"%s %s %s %s (%s)"
        (hash, asunto.length > 50 ? asunto[0..48] ~ ".." : asunto, padre, correo, registrador == correo ? registrador : "mismo");
    }

    this(string h, bool completar = false, string gitRepo = "") {
        this.hash = h;
        if (completar) {
            Commit tmp = dameUnCommit(gitRepo, h);
            this = tmp;
        }
    }

    bool opEquals(ref Commit otro, bool detallada = false) {
//imported!"std.stdio".writeln("Commit op equals ", detallada, ", ", this, "\notro ", otro);
        if (!detallada) {
            return this.hash == otro.hash;
        }
        if ((this.hash != otro.hash) ||
            (this.fechaCommit != otro.fechaCommit) ||
            (this.asunto != otro.asunto) ||
            (this.árbol != otro.árbol) ||
            (this.padre != otro.padre) ||
            (this.correo != otro.correo) ||
            (this.registrador != otro.registrador) ||
            (this.fechaRegistro != otro.fechaRegistro) ||
            (this.diferencias != otro.diferencias)
           ) {
//imported!"std.stdio".writeln("Commit op equals, no son igual; ", this, "\n", otro);
            return false;
        }
//imported!"std.stdio".writeln("Commit op equals, es igual");
        return true;

    }
}

Commit dameUnCommit(string gitRepo, string hash) {
    import std.process : execute, Config;
    import std.array : array, split;
    import std.file : DirEntry, dirEntries, SpanMode, isFile;
    import std.algorithm : filter, map;
    import std.string : splitLines, strip;
    import std.conv : to;
    import std.range : drop;

    string delimiter = "-+-+-+-+-+CHANGELOG-CREATOR-DELIMITER+-+-+-+-+";
    string prettyOpt = "--pretty=format:" ~ delimiter ~
        "%n<ASUN>%s<FINASUN>" ~
        "%n<HASH>%H<FINHASH>" ~
        "%n<FECHACOMMIT>%aI<FINFECHACOMMIT>" ~
        "%n<PADRE>%P<FINPADRE>" ~
        "%n<ARBOL>%T<FINARBOL>" ~
        "%n<CORREOAUTOR>%ae<FINCORREOAUTOR>" ~
        "%n<CORREOREGISTRADOR>%ce<FINCORREOREGISTRADOR>" ~
        "%n<FECHAREGISTRADOR>%cI<FINFECHAREGISTRADOR>";
    auto showProc = execute(["git", "--git-dir=" ~ gitRepo, "show", prettyOpt, "--numstat", hash], null, Config.none, ulong.max, gitRepo);
    assert(showProc.status == 0, "Git show failed: " ~ showProc.to!string ~ ". Perhaps git pull needed?");

    string commit = showProc.output
        .strip
        .split(delimiter)
        .filter!(str => str.length > 0)
        .front;

    auto campos = ["ASUN", "HASH", "FECHACOMMIT", "PADRE", "ARBOL", "CORREOAUTOR", "CORREOREGISTRADOR", "FECHAREGISTRADOR"];

    import std.datetime : DateTime;
    import std.string : indexOf;

    string extraer(string token) {
        auto ext = commit[commit.indexOf("<" ~ token ~ ">") + token.length + 2.. commit.indexOf("<FIN" ~ token ~ ">")];
        return ext;
    }

    static immutable dateFmtLength = "YYYY-MM-DDTHH:MM:SS".length;

    Commit ret = Commit(
        asunto: extraer("ASUN"),
        hash: extraer("HASH"),
        fechaCommit: DateTime.fromISOExtString(extraer("FECHACOMMIT")[0 .. dateFmtLength]),
        padre: extraer("PADRE").split(" "),
        árbol: extraer("ARBOL"),
        correo: extraer("CORREOAUTOR"),
        registrador: extraer("CORREOREGISTRADOR"),
        fechaRegistro: DateTime.fromISOExtString(extraer("FECHAREGISTRADOR")[0 .. dateFmtLength]),
        diferencias: commit[commit.indexOf("FINFECHAREGISTRADOR>") + 20 .. $].strip.splitLines.ordenaDiferencias
    );

    return ret;
}
