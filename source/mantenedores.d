module mantenedores;

struct Entrada {
    char tipo; // F
    string expre; // regex
}
struct ÁreaMantenida {
    string nombre;
    Entrada[] entradas;
}
   
ÁreaMantenida[] parseMaintainers(string path) {
    import std.stdio : File;

    ÁreaMantenida[] ret;

    auto buf = File(path);
    string title = "";
    /*
     * The maintainers file does not have any formal format
     * We detect the "title" by waiting for a newline, then we set the title variable to the next line
     * After a void line the title is resetted again
     */
    foreach(line; buf.byLine) {
        import std.regex : regex, matchFirst;
        import std.conv : to;
        import std.array : split;
 
        if (line == "") {
            title = "";
            continue;
        }

        if (title == "") {
            title = line.to!string;
            continue;
        }

        auto expr = regex(`^([A-Z]):\s*(.*)`);
        auto match = matchFirst(line, expr);

        ÁreaMantenida fill;
        fill.nombre = title;

        if (match.empty == false) {
            import std.conv : to;

            string[] res = split(match.hit.to!string, "\t");
            // Some entries use whitespaces instead of tabs
            if (res.length != 2)
                continue;

            Entrada tmp;
            // M:<tab>blah
            tmp.tipo = line[0];
            tmp.expre = line[3..$].to!string;
            fill.entradas ~= tmp;
        }
        ret ~= fill;
    }
    return ret;
}


ÁreaMantenida[] filterMaintainers(ÁreaMantenida[] áreas, char[] keys) {
    import std.algorithm : filter, any;
    import std.array : array;

    ÁreaMantenida[] ret;
    foreach(área; áreas) {
        ÁreaMantenida fill;
        fill.nombre = área.nombre;
        fill.entradas = área.entradas.filter!(ent => keys.any!(k => k == ent.tipo)).array;
        if (fill.entradas.length > 0)
            ret ~= fill;
    }
    return ret;
}


ÁreaMantenida[] seccionaYFiltraArchivos(string repo) {
    import std.path : buildPath;
    
    string path = buildPath(repo, "MAINTAINERS");

    return parseMaintainers(path)
        .filterMaintainers(['F']);
}
